<?php
session_name("immobilienberechner");
session_start();

if (strlen(session_id()) != 32)
{
  mt_srand ((double)microtime()*1000000);
  session_id(md5(uniqid(mt_rand())));
}
setcookie(session_name(), session_id());
/*
*
*  global functions
*
*/


require("mycrm/cfg/config.php");
require("mycrm/lib/class.connect.php");

$db_sets = new main_class;    // class.connect.php

$db_sets->database=$dbname;
$db_sets->host=$hostname;
$db_sets->user=$dbuser;
$db_sets->password=$dbpass;

$db_sets->connect();



/*
*  user functions 
*/

/*
include "mycrm/funcs/admin_funcs.php";
include "mycrm/funcs/calendar_funcs.php";
include "mycrm/funcs/google_funcs.php";
include "mycrm/funcs/rewrite_funcs.php";
include "mycrm/funcs/safe_funcs.php";
*/


include "mycrm/funcs/class_auth_funcs.php";
include "mycrm/funcs/class_register_funcs.php";
include "mycrm/funcs/user_funcs.php";

?>