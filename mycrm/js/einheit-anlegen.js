$( document ).ready(function() { 		
	
//***********FORMFELDER HINZUFÜGEN
		$(".js-add-form-fields").on('click', function(ev){
			ev.preventDefault();
			addFormField();
			initFormFields();
		});
	
		function addFormField() {
			
			var id = $('#id').val();
			
			var fields = '<div class="grid_12 ins7"><div class="grid_11"></div><div class="grid_1"><h3 class="rot"><a href="" class="js-remove-form-fields"><i class="fa fa-minus-square fa-2x" aria-hidden="true"></i></a></h3></div><div class="grid_12 grid_row"><div></div><div class="grid_4"><label for="zusatz_art_'+id+'">Objektart</label><div><select name="zusatz_art_'+id+'" ><option value="">Objektart wählen</option><option value="stellplatz">Stellplatz</option><option value="carport">Carport</option><option value="garage">Garage</option><option value="tiefgarage">Tiefgarage</option></select></div></div><div class="grid_4"><label for="zusatz_preis_'+id+'">Kaufpreis</label><div class="euro"><input type="text" name="zusatz_preis_'+id+'" placeholder="0,00"></div></div><div class="grid_4"><label for="zusatz_afa_'+id+'">Abschreibung des Kaufpreises</label><div><select name="zusatz_afa_'+id+'"><option value="">Abschreibung wählen</option><option value="einheit">AfA wie Einheit</option></select></div></div></div>';
			
			$("#addFields").append(fields);
			$('#id').val(++id);
		}
		
		$(".js-add-form-fields-2").on('click', function(ev){
			ev.preventDefault();
			addFormField2();
			initFormFields();
			initShowErweiterung();
		});
		
		function initFormFields(){
			$("select:not([class*='ui-datepicker'])").on('change', function(ev){
				setSelectBox(ev);	
			});
			
			initRemoveFormFields();
			$("select:not([class*='ui-datepicker']):not(#select_qm)").change();
		}
		
		function addFormField2() {
			
			var id = $('#id_2').val();
			
			var fields = '<div class="grid_12 ins7"><div class="grid_11"></div><div class="grid_1"><h3 class="rot"><a href="" class="js-remove-form-fields"><i class="fa fa-minus-square fa-2x" aria-hidden="true"></i></a></h3></div><div class="grid_12 grid_row"><div></div><div class="grid_5"><label for="lauf_nebenkosten_bezeichnung_'+id+'">Bezeichnung</label><div><input type="text" name="lauf_nebenkosten_bezeichnung_'+id+'" placeholder="Bezeichnung"></div></div><div class="grid_2"><label for="lauf_nebenkosten_abzugsfaehig_'+id+'">Steuerlich Abzugsfähig</label><div><select name="lauf_nebenkosten_abzugsfaehig_'+id+'"><option value="">Wählen</option><option value="ja">Ja</option><option value="nein">Nein</option></select></div></div><div class="grid_3"><label for="lauf_nebenkosten_qm_'+id+'">Standartwert in € oder €/m<sup>2</sup></label><div class="einheit"><input type="text" name="lauf_nebenkosten_qm_'+id+'" placeholder="0,00"><div class="select-wrapper"><select name="lauf_nebenkosten_qm_einheit_'+id+'" id="select_qm_'+id+'"><option value="euro">€</option><option value="euro_qm">€/m2</option></select></div></div></div><div class="grid_2"><br><button type="button" class="btn14 js-show-erweiterung show-erweiterung">Erweiterung</button></div><div class="grid_12 ins7 grid_row js-erweiterung erweiterung"><div></div><div class="grid_3"><label for="erweiterung_nebenkosten_summe_'+id+'">Summe Standartwert</label><div class="euro"><input type="text" name="erweiterung_nebenkosten_summe_'+id+'" placeholder="0,00"></div></div><div class="grid_3"><label for="erweiterung_nebenkosten_steigerung_ab_'+id+'">Steigerung ab Jahr</label><select name="erweiterung_nebenkosten_steigerung_ab_'+id+'"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div><div class="grid_3"><label for="erweiterung_nebenkosten_steigerung_alle_'+id+'">Steigerung alle Jahr</label><select name="erweiterung_nebenkosten_steigerung_alle_'+id+'"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div><div class="grid_3"><label for="erweiterung_nebenkosten_steigerung_'+id+'">Steigerung in € oder %</label><div class="einheit"><input type="text" name="erweiterung_nebenkosten_steigerung_'+id+'" placeholder="0,00"><div class="select-wrapper"><select name="erweiterung_nebenkosten_steigerung_einheit_'+id+'"><option value="euro">€</option><option value="prozent">%</option></select></div></div></div></div></div></div>';
			
			$("#addFields2").append(fields);
			$('#id_2').val(++id);
		}
		
		$(".js-add-form-fields-3").on('click', function(ev){
			ev.preventDefault();
			addFormField3();
			initFormFields();
			$(".js-bezugsgroesse , .js-bezugsgroesse-proz").on( "focusout", function(){
				calcBetrag(this);
			});
		});
		
		function addFormField3() {
			
			var id = $('#id_3').val();
			
			var fields = '<div class="grid_12 ins7">'+
							'<div class="grid_11"></div>'+
							'<div class="grid_1"><h3 class="rot"><a href="" class="js-remove-form-fields"><i class="fa fa-minus-square fa-2x" aria-hidden="true"></i></a></h3></div>'+
							'<div class="grid_12 grid_row">'+
								'<div></div>'+
								'<div class="grid_4"><label for="erwerbskosten_bez_'+id+'">Bezeichnung</label><input type="text" name="erwerbskosten_bez_'+id+'" placeholder="Bezeichnung"></div>'+
								'<div class="grid_4"><label for="erwerbskosten_typ_'+id+'">Tp</label><div><select name="erwerbskosten_bez_'+id+'" ><option value="">Typ wählen</option><option value="werbungskosten">Werbungskosten</option></select></div></div>'+
								'<div class="grid_2"><label for="erwerbskosten_jahr_'+id+'">Jahr</label><input type="text" name="erwerbskosten_jahr_'+id+'" placeholder=""></div>'+
								'<div class="grid_2"><label for="erwerbskosten_proz_'+id+'">Prozent</label><div class="proz"><input type="text" name="erwerbskosten_proz_'+id+'" placeholder="0,00" value="100"></div>  </div>'+
								'<div class="grid_12 ins7 grid_row">'+
									'<div></div>'+
									'<div class="grid_3"><label for="erwerbskosten_bezugsgroesse">Bezugsgröße</label><div class="euro"><input type="text" name="erwerbskosten_bezugsgroesse" placeholder="0,00" class="js-bezugsgroesse"></div></div>'+
									'<div class="grid_3"><label for="erwerbskosten_bezugsgroesse_proz">Prozent</label><div class="proz"><input type="text" name="erwerbskosten_bezugsgroesse_proz" placeholder="0,00" class="js-bezugsgroesse-proz"></div></div>'+
									'<div class="grid_3"><label for="erwerbskosten_betrag">Betrag</label><div class="euro"><input type="text" name="erwerbskosten_betrag" placeholder="0,00" class="js-bezugsgroesse-betrag" readonly=""></div></div>'+
								'</div> '+
							'</div>';
			
			$("#addFields3").append(fields);
			$('#id_3').val(++id);
		}
		
		function initShowErweiterung(){
			$(".js-show-erweiterung").off('click');
			$(".js-show-erweiterung").on('click', function(ev){
				toggleClass(ev);
			});
		}
		
		function initRemoveFormFields(){
			$(".js-remove-form-fields").off('click');
			$(".js-remove-form-fields").on('click', function(ev){
				removeFormField(ev);
			});
		}
		
		function removeFormField(ev){
			ev.preventDefault();
			$(ev.target).closest('div.grid_12').remove();
		}
		
//*********FARBE ÄNDERN
		function setSelectBox(ev){
			var selectBox = $(ev.target);
			
			if($(selectBox).val() == ''){
				$(selectBox).css('color', '#d9d9d9');
			} else {
				$(selectBox).css('color', '#888')
			}
		}
		
//*********SELECT BOX SMALL
		$("#select_qm").on('change', function(ev){
			$("#select_qm").toggleClass( "small" );	
		});
		
//*********ERWEITERUNG ANZEIGEN
		$(".js-show-erweiterung").on('click', function(ev){
			toggleClass(ev);
		});
		
		function toggleClass(ev){
			$(ev.target).toggleClass( "delete" );
			$(ev.target).parent().parent().find('.js-erweiterung').toggleClass( "active" );	
		}
		
//*********KACHELN AKTIVIEREN
		setKachel(2);
		setKachel(3);
		
		function setKachel(stepNumber){
			var element = $.find(".js-step[data-step='"+stepNumber+"']");
			$(element).removeClass('js-no-action');
			$(element).addClass('js-action');
			$(element).find('.box_aside').addClass('active');
			var img = $(element).find('img').data('img');
			$(element).find('img').attr('src', 'images/kacheln-einheit/'+img+'_weiss.svg');
		}
		
		$(".js-no-action").on('click', function(ev){
			ev.preventDefault();
		});
		
		$(".js-action").on('click', function(ev){
			changeKachel(ev);
		});
		
		function changeKachel(ev){
			ev.preventDefault();
			var number = $(ev.target).closest('a').data('step');
			$("section[id*='form-step-']").removeClass('active');
			$("section#form-step-"+number).addClass('active');
		}
		
//*********BETRAG BERECHNEN
		$(".js-bezugsgroesse , .js-bezugsgroesse-proz").on( "focusout", function(){
			calcBetrag(this);
		});
		
		function calcBetrag(element){
			if($(element).hasClass('js-bezugsgroesse')) {
				var bezug = $(element).val(), 
					proz = $(element).closest('div.grid_12').find('input.js-bezugsgroesse-proz').val();
			} 
			else if($(element).hasClass('js-bezugsgroesse-proz')) {
				var proz = $(element).val(), 
					bezug = $(element).closest('div.grid_12').find('input.js-bezugsgroesse').val();
			}
			
			if(bezug != '' && proz != ''){
				bezug = bezug.replace('.', '');
				bezug = bezug.replace(',', '.');
				proz = proz.replace(',', '.');
				var betrag = bezug * (proz / 100);
				$(element).closest('div.grid_12').find('input.js-bezugsgroesse-betrag').val(numberWithCommas((betrag.toFixed(2)).replace('.', ',')));
			}
		}
		
		function numberWithCommas(x) {
			return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		}
		
//*********TABELLE BEFÜLLEN
		$(".js-set-table:not(.delete)").on('click', function(ev){
			var $container = $(ev.target).closest('div.grid_12'),
				miete = $container.find("input[name='gesamtmiete']").val();
			$container.find('table td input[type="text"]').val(miete);
			
			var monat = $("#nutzungsbeginn_monat_db").data('value');
			
			for(var i = 2; i <= monat ; i++){
				var element = $container.find("table tbody tr:first-child td:nth-child("+i+") input");
				element.remove();
			}
			
			
			$('select[name="miete_steigerung_monat"]').val(monat);
			$('select[name="miete_steigerung_monat"]').data('select', parseInt(monat) - 1);
		});
		
		$(".js-fill-table").on('click', function(ev){
			var $container = $(ev.target).closest('div.grid_12').parent()
			
			var intervall = $container.find('input[name="miete_steigerung_x"]').val(),
				jahr = $container.find("select[name='miete_von']").val(),
				jahrX = $container.find("select[name='miete_bis']").val(),
				miete = $container.parent().find("input[name='gesamtmiete']").val(),
				monat = $container.find("select[name='miete_steigerung_monat']").val();
				
			if(miete == ''){
				miete = '0,00';
			}
			miete = miete.replace('.', '');
			miete = miete.replace(',', '.');
			miete = parseInt(miete);
			
			jahr = parseInt(jahr);
			jahrX = parseInt(jahrX);
			
			intervall = parseInt(intervall);
			
			var jahrNext = jahr + intervall;
			
			for(var x = jahr; x <= jahrX; x++) {
				var element = $container.find("table tr[data-jahr='"+x+"']");
				
				if(x == jahrNext && intervall > 0) {
					
					var m = parseInt(monat) + 1;
					$(element).find('td input').val(numberWithCommas((miete.toFixed(2)).replace('.', ',')));
					
					miete = getSteigerung($container, miete);
					for(m ; m <= 13 ; m++) {
						$(element).find('td:nth-child('+m+') input').val(numberWithCommas((miete.toFixed(2)).replace('.', ',')));
					}
					
					jahrNext = jahrNext + intervall;
				} else {
					$(element).find('td input').val(numberWithCommas((miete.toFixed(2)).replace('.', ',')));
				}
			}
			
		});
		
		$(".js-clear-tr").on('click', function(ev){
			$(ev.target).closest('tr').find('td input').val('0,00');
		});
		
		function getSteigerung(container, miete){
			
			var valueEuro = $(container).find("input[name='miete_steigerung_euro']").val(), 
				valueProz = $(container).find("input[name='miete_steigerung_proz']").val(); 
				
			if( valueEuro != '' ) {
				var steigerung = valueEuro;
				steigerung = getFloatSteigerung(steigerung);
				miete = miete + steigerung;
			} 
			else if(valueProz != ''){
				var steigerung = valueProz;
				steigerung = getFloatSteigerung(steigerung);
				if(steigerung > 0) {
					miete = miete * ((steigerung / 100) + 1);
				}
			} 
			
			return miete;
		}
		
		function getFloatSteigerung(steigerung){
			steigerung = steigerung.replace('.', '');
			steigerung = steigerung.replace(',', '.');
			steigerung = parseFloat(steigerung);
			
			return steigerung;
		}
		
		$("input[name='miete_steigerung_euro']").on('focusout', function(ev){
			var $container = $(ev.target).closest('div.grid_12');
			var value = $container.find("input[name='miete_steigerung_euro']").val();
			if(value != '' && value > 0 && !isNaN(value)) {
				$container.find("input[name='miete_steigerung_proz']").val('');
				value = value.replace('.', '');
				value = value.replace(',', '.');
				value = parseFloat(value);
				$container.find("input[name='miete_steigerung_euro']").val(numberWithCommas((value.toFixed(2)).replace('.', ',')));
			} else if(isNaN(value)) {
				$container.find("input[name='miete_steigerung_euro']").val('');
			}
		});
		
		$("input[name='miete_steigerung_proz']").on('focusout', function(ev){
			var $container = $(ev.target).closest('div.grid_12');
			var value = $container.find("input[name='miete_steigerung_proz']").val();
			if(value != '' && value > 0 && !isNaN(value) ) {
				$container.find("input[name='miete_steigerung_euro']").val('');
				value = value.replace('.', '');
				value = value.replace(',', '.');
				value = parseFloat(value);
				$container.find("input[name='miete_steigerung_proz']").val(numberWithCommas((value.toFixed(2)).replace('.', ',')));
			}else if(isNaN(value)) {
				$container.find("input[name='miete_steigerung_proz']").val('');
			}
		});
		
		$(".js-show-erweiterung-plus").on('click', function(ev){
			ev.preventDefault();
			$(ev.target).closest('div.grid_12').toggleClass('delete-plus');
		});
		
		$(".js-refresh").on('click', function(ev){
			var $container = $(ev.target).closest('div.grid_12');
				$container.find('input[type="text"]').val('');
				$container.find('select').each(function()
				{
					var valueSelect = $(this).data('select');
					
					if(valueSelect == 'last') {
						valueSelect = $(this).find('option').length;
						valueSelect--;
					} 
					
					$(this)[0].selectedIndex=valueSelect;
					
				});
		});
		
//*********RESPONSIVE TABLE
		$('.js-resp-table').footable(); 
		
		if(window.outerWidth < 768) {
			addListenerClear();
		} else {
			window.addEventListener('resize', addListenerClear);
		}
		
		function addListenerClear(){
			var w = window.outerWidth;
			if(w < 768){
				$(".js-clear-tr").on('click', function(ev){
					var $table = $(ev.target).closest('tr').closest('table');
					if($table.hasClass('footable-details')){
						$(ev.target).closest('tr').closest('table.footable-details').find('td input').val('0,00');
					} else {
						$(ev.target).closest('tr').find('td input').val('0,00');
					}
				});
				window.removeEventListener('resize', addListenerClear);
			}
		}
		

}); 