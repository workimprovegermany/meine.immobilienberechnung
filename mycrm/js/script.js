$( document ).ready(function() { 		
	$("select:not([class*='ui-datepicker'])").on('change', function(ev){
		setSelectBox(ev);	
	});
	
	$("select:not([class*='ui-datepicker'])").change();
	
	function setSelectBox(ev){
		var selectBox = $(ev.target);
		
		if($(selectBox).val() == ''){
			$(selectBox).css('color', '#d9d9d9');
		} else {
			$(selectBox).css('color', '#888')
		}
	}
	
	$(".js-delete").click(function(){
		$(this).parent().parent().parent().find(':input').val('');
		$("select:not([class*='ui-datepicker'])").change();
	});
	
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd.mm.yy"
	});
	
	$(".js-calendar").click(function(){
		$(this).parent().find('input').focus();
	});
}); 