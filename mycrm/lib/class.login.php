<?php

/*
* 
* @ Curelea / Meier
*  
*/

class Login 
{

 var $user;
 var $pass;

 var $tries; 
 var $sec_mode;
 
 var $max_user_length = "100";
 var $max_pass_length = "32";
 
 var $not_allowed_user_chars = array("'",";",":","\\"," ");

 function Login($secure)
 {
 	if ($secure == "1")
 	{
 		$this->sec_mode = "1"; // md5
 	}	
 	else
 	{
 		$this->sec_mode = "0"; // md5
 	}	
 }	

 function set_login_values($user,$pass)
 {
   $this->user = trim($user);
   
    if ($this->sec_mode == "0")
    {
     $this->pass = trim($pass);
    }
    else
    {
     $this->pass = md5(trim($pass));
    }	  
   
   if ($this->check_values() == "0")
   {
  	 $this->not_valid("Login fehlgeschlagen","konto-user.html");
   }	
 }
 
 /*
 *
 * Eingabe werte pr�fen
 *
 */
 
 function check_values()
 {
 	
 	if ($this->user == "" || $this->pass == "") 
 	{  
	 return 0; // failed no entries	
  }	
  
   
 	if (strlen($this->user) > $this->max_user_length || 
 	    strlen($this->pass) > $this->max_pass_length)
 	{  
	 return 0; // failed to long entries	
  }	
  
  // User Eingaben auf erlaubte Zeichen pruefen 
  
  for($i=0;$i < strlen($this->user); $i++)
  {
  	 if (in_array($this->user{$i}, $this->not_allowed_user_chars))
  	 {
  	 	return 0; // unerlaubte zeichen im Userfeld .....
  	 }	
  }	    
 
 }	
 
 // !be carefull an deactived this function on your website
 
 /*
 function get_login_user()
 {
 	return $this->user;
 }
 	
 function get_login_pass()
 {
 	return $this->pass;
 }
 */
 
 
 // 
 function check_login()
 {
 	  
  if(!get_user($this->user)) 
  {
  	 $this->not_valid("Login fehlgeschlagen","konto-user.html");
  	 return 0;
  }    	 

  if(!check_user_pass($this->user, $this->pass)) 
  {
   	 $this->not_valid("Login fehlgeschlagen","konto-user.html");
   	 return 0;
  }  
  else
  {
 	 return 1;
  }	
 }
 
 function not_valid($string,$url)
 {
    redirect("konto-user.html");
    exit("");
 }	


} // end of class

?>