<?php

class main_class 
{

  var $host     = ""; // host where the db is running
  var $user     = ""; // user of the db
  var $password = ""; // pass of the db

  var $database = ""; // databasename

  var $link_id  = 0; // connection	( 0 = connect, 1 = no connect)
  var $query_id = 0; // anweisungs_nummer

  var $valid = 0;
  var $showerror = 1;

// connect to database
function connect() 
{
 if (0==$this->link_id) 
 {
   $this->link_id=@mysql_connect($this->host, $this->user, $this->password);
   if (!$this->link_id) 
   {
    $this->halt_on_error("<br /><br />connection failed to $this->host <br />");
   }
   if ($this->database!="") 
   {
    if(!mysql_select_db($this->database, $this->link_id)) 
    {
     $this->halt_on_error("<h2> cannot use database </h2>".$this->database);
    }
   }
  }
} 

function tbl_exists($tablename) 
{
   // Get a list of tables contained within the database.
   $result = mysql_list_tables($this->database);
   $rcount = mysql_num_rows($result);
 
   // Check each in list for a match.
   for ($i=0;$i < $rcount;$i++) 
   {
     if (mysql_tablename($result, $i)==$tablename) return true;
   }
   return false;
}

// get query
function query($query_string) 
{
	  // abfrage 
   $this->query_id = mysql_query($query_string,$this->link_id);
   if (!$this->query_id) 
   {
    $this->halt_on_error("<h2> Invalid SQL-Syntax: </h2>".$query_string);
   }
   return $this->query_id;
} 

function log_query($query_string)
{
   $this->query_id = mysql_query($query_string,$this->link_id);
   if (!$this->query_id)
   {
    $this->halt_on_error("<h2> Invalid SQL-Syntax: </h2>".$query_string);
   }
   
   if (_LOG_DB_ == "1")
   {
    $this->log_sql_query($query_string);
   } 
   return $this->query_id;
}

function fetch_array($query_id=-1) 
{
  if ($query_id!=-1) 
  {
   $this->query_id=$query_id;
  }
  $eff_row = mysql_fetch_array($this->query_id);
  return $eff_row;
}

// get the rows of the query
function num_rows($query_id=-1) 
{
  if ($query_id!=-1) 
  {
    $this->query_id=$query_id;
  }
  return mysql_num_rows($this->query_id);
}

// get the rows of the query
function server_stat($query_id=-1) 
{
  if ($query_id!=-1) 
  {
   $this->query_id=$query_id;
  }
  return mysql_stat($this->link_id);
}

// clean mysql 
function escape_smart($value) 
{
  if (get_magic_quotes_gpc()) 
  {
   $value = stripslashes($value);
  }
  if (!is_numeric($value)) 
  {
   $value = mysql_escape_string($value);
  }
  return $value;
}

// reports the error
function halt_on_error($msg) 
{
  $this->errdesc=mysql_error();
  $this->errno=mysql_errno();
  $msg .="<br /><i>mysql error: $this->errdesc</i><br />";
  $msg .="<i>mysql error number: $this->errno</i><br />";
  $msg .= "<br />";

  if ($this->showerror==1) 
  {
    echo "$msg<br />\n";
    exit("Connection error...plz be patient");
  }
}

function log_sql_query($sql_string)
{
 global $GLOBALS_;
	
 $date_     = date("d.m.Y H:i:s");
 $log_str   = $date_ . " | " . $sql_string . "\n";
 $sql_file  = $GLOBALS_['path']['sql']."sql_dml.php";

 // Sichergehen, dass die Datei existiert und beschreibbar ist
 if (!$handle = fopen($sql_file, "a+"))
 {
  echo "Kann die Datei $sql_file nicht &ouml;ffnen";
  exit;
 }
 if (!fwrite($handle, $log_str))
 {
  echo "Kann in die Datei $sql_file nicht schreiben";
  exit;
 }
 fclose($handle);
}

}//class ends



?>