<?php

/*
*
*   Session - Register 
*
*/

class Register 
{

   var $session_id;
   var $ip;
   var $base;
 
   var $is_only_char    = "0";

   var $easy_string     = array ( "test", "pass", "none", "1234", 
                                  "admi", "user", "down", "qwer", 
                                  "5678", "2001", "1980", "1990", 
                                  "1970", "1960" );
   /* Functions */                                  
                                     
   function set_session_id($value)
   {
    $this->session_id = $value;
   }

   function set_ip($value)
   {
    $this->ip = $value;
   }

   function set_base($value)
   {
    $this->base = $value;
   }

   // auf session_id pruefen, doppelt oder nicht ?

   function check_id ($sess_id,$ip,$base) 
   {
     
     $this->set_session_id($sess_id);
     $this->set_ip($ip);
     $this->set_base($base);
     $this->lang = "1"; // get_online_language_to_session_id($sess_id);
     
     if ($this->lang == "0" || $this->lang == "")
     {
     	 $this->lang = "1";
     }
     
     $id = select_online_id ($this->session_id);

     if ($id < 1)
     {
       $this->add_id();
     }
     elseif ($id > 2)
     {
      $this->delete_id();
     }
     else
     {
      $this->update_id();
     }
       
     $this->timeout_session();

     }
     // add a session,  id, ip, time, in table online adden
     function add_id()
     {
	    $time    = date("Y-m-d H:i:s");
      insert_online_session_id($this->session_id,$this->ip,$this->base,$time);
     }
     // delete a session
     function delete_id()
     {
      remove_online_session_id($this->session_id);
     }
     // update a session
     function update_id()
     {
      $time    = date("Y-m-d H:i:s");
      update_online_session_id($this->session_id,$this->ip,$this->base,$time);
     }

     /*
     *  Timeout -> wenn eine Session 30 min nicht mehr upgedated wurde User
     *  offline schicken.....
     *  time = einstellbar
     */

    function timeout_session()
    {
   	 $expires = 1200; // 3600 sec = 1h -> 1200 = 20 min -> 1800 -> 30 min
  	 $get_id  = select_time_from_online($expires);
     // echo "<strong>TIMEOUT : $get_id</strong><br />";
	   // $logout = update_surf_data_last_visit($auth->uid,$heute); // Hab noch gar kein $auth->irgendwas
     if (is_numeric($get_id) && $get_id > 0)
     {
      $del_id = delete_id_from_online($get_id);
      $this->log_out();
     }
    } // function ends

    function log_out()
    {
      session_destroy();
	  }
	  
    function is_only_char($my_string) 
    {
     
     if (strlen($my_string) > 50)
     {
     	 return 0;
     }	
     else
     {
      $my_string = strtolower($my_string);

      for ($i=0;$i < strlen($my_string); $i++) 
      {
       $ascii_code=ord($my_string[$i]);
 
       if (intval($ascii_code) >=97 && intval($ascii_code) <=122) 
       {
        continue;
       }
       else 
       {
        // $this->is_only_char = 1;
        return 0;
       }
      }
      return 1; //$this->is_only_char;
     }
    }
	  

} // of class
?>