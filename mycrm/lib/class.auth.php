<?php

/*
*
*  Auth - Klasse 
*
*/


class Auth 
{

  var $logged_in;      // NOT SURE ? 
  
  var $function;       // Status.....
  var $name;           // Name 
  var $nname;          // NachName 
  var $nvname;         // Vorname 
  var $uname;          // LoginName 
  var $uid;            // uid
  //var $gid = array();  // gid
  var $active;         // Account active ?
  var $prio;           // Prio Account
  var $status;         // Status
  var $lang;           // Language
  
  var $bid;           // id des aktuellen Datensatzes
  var $such1;           // id des aktuellen Datensatzes
  var $such2;           // id des aktuellen Datensatzes
  var $such3;           // id des aktuellen Datensatzes
  var $such4;           // id des aktuellen Datensatzes
  var $such5;           // id des aktuellen Datensatzes
  
  var $email;           // E-Mail Daten
  var $anrede;        // Anrede
  var $firma;           // Firma


  function check_user($value) 
  {
    global $db_sets;
    // uname zu session_id holen
    $args = get_name_after_login($value);
    
    $this->uname = $args[0];
    $this->lang  = $args[3];
    
    if ($this->uname) 
    {
        $this->logged_in = 1;

     	$auth_1 = get_auth_data($this->uname);
        while($get = $db_sets->fetch_array($auth_1))
        {
      	 $this->uid      = intval($get['uid']);
      	 $this->prio     = intval($get['prio']);
      	 $this->active   = intval($get['aktiv']);
      	} 
      	
      	$auth_2 = get_more_auth_data($this->uid);
        while($get = $db_sets->fetch_array($auth_2))
      	{
        	$this->vname   = utf8_encode($get['vname']);
        	$this->nname   = utf8_encode($get['name']);
        	$this->anrede   = $get['anrede'];
        }
		if($this->anrede == 1)
		{
			$this->anrede = "Herr";
		}
		else
		{
			$this->anrede = "Frau";
		}
        
        $this->name = $this->nname.", ".$this->vname;
        
		
      	$auth_3 = get_auth_user_aktuell($this->uid);
        while($get = $db_sets->fetch_array($auth_3))
      	{
        	$this->bid   = $get['b_id'];
        	$this->such1   = $get['such_01'];
        	$this->such2   = $get['such_02'];
        	$this->such3   = $get['such_03'];
        	$this->such4   = $get['such_04'];
        	$this->such5   = $get['such_05'];
        }
		
		
      	$auth_4 = get_auth_user_firma($this->uid);
        while($get = $db_sets->fetch_array($auth_4))
      	{
        	$this->firma  = utf8_encode($get['firma']);
        }
		
        /*
        $auth_4 = get_status_auth_data($this->uid);
        while($get = $db_sets->fetch_array($auth_4))
      	{
          $this->status   = $get['status'];
      	  $this->function = get_geno_function_to_id($this->status);
        }
	
        $auth_6 = get_more_auth_email_data($this->uid);
        while($get = $db_sets->fetch_array($auth_6))
		{
          $this->email   = $get['email'];
		}  
		*/
    }	
    else 
    {
      $this->logged_in = 0;
      return false;
    }
   } 

} // class ends
?>










