<?php
define("_BASE_","einheit-anlegen.php");  // filename (basename perl&php);
$stamp = filemtime(_BASE_);
$last_touch =  date("d.m.Y", $stamp);
$stunde  = date("H");
function utime ()
{
$time = explode( " ", microtime());
$usec = (double)$time[0];
$sec = (double)$time[1];
return $sec + $usec;
}
$start = utime();
$stamp = time();
$heute = gmdate("d m Y H:i:s" , $stamp);
$tag  = date("d");
$monat  = date("m");
$jahr  = date("Y");
$uhr  = date("G");
$minute  = date("i");
$datum = $tag.".".$monat.".".$jahr;
$datum_01 = $jahr."-".$monat."-".$tag;


require("global_funcs.php");
require("inc/lib_incl_intern.php");
?>

<?php

/*
if (!defined('_IS_VALID_')  || $auth->prio <= "0")
{
  echo "Unerlaubter Zugriff.....";
  echo "</body>";
  echo "</html>";
  exit();
}

else
{*/
include "inc/head.inc.php";
include "inc/header.php";

// Hier wird die Nav nach der Prio  aus gegeben ! 
include "nav/nav.php";

//}


 

?>


<!-- ==============CONTENT============== -->
  
      <main class='einheit-anlegen'>
      

<?php

		echo "<section class=\"well1\">";
			echo "<div class=\"container-fluid\">";
			
				//Variablen
				$vtext01 = 'denkmal'; 
				$vtext02 = 'images/test/immobilie_1.jpg'; //Pfad Bild von Immobilie
				$vtext03 = 'Denkmalobjekt'; //Objektart
				$vtext04 = 'Sachsen'; //Bundesland
				$vtext05 = 'Ludwig von Annaberg Str. 17'; //Straße
				$vtext06 = 'Leipzig'; //Ort
				$vtext07 = 'xy'; //Bauträger
				
				echo "<div class='immobilie einheit-anlegen'>";
					echo "<div class='grid_2 bild-container'>";
						echo "<div class='$vtext01' style='background-image: url($vtext02)'>";
							echo "<img src='$vtext02' />";
						echo "</div>";
					echo "</div>";
					
					echo "<div class='grid_10 info-container'>";
					
						echo "<div class='grid_3 info'>";
							echo "<img class='icon_haus' src='images/haus.svg' />";
							echo "<div>";
								echo "<p>$vtext03</p>";
								echo "<p>$vtext04</p>";
							echo "</div>";
						echo "</div>";	
						
						echo "<div class='grid_3 info'>";
							echo "<p>$vtext05</p>";
							echo "<p><img class='icon_ort' src='images/standort.svg' /> <strong>$vtext06</strong></p>";
						echo "</div>";
							
						echo "<div class='grid_3 info'>";
							echo "<p>Bauträger $vtext07</p>";
						echo "</div>";
							
						echo "<div class='grid_3 info'>";
							echo "<h4 class='fa'>";
								echo "<a href='immobilien.php' title='Zurück'>";
									echo "<i class='fa fa-times-circle fa-lg' aria-hidden='true' title='zurück'></i>";
								echo "</a>";
							echo "</h4>	";
						echo "</div>";
							
					echo "</div>"; //ENDE grid_10
					
					
				echo "</div>";
			echo "</div>";
		echo "</section>";
		
		echo "<section>";
			echo "<div class=\"container-fluid\">";
				echo "<ul class=\"row product-list\">";

					echo "<a href=\"\" class='js-step js-action' data-step='1'>";
						echo "<li class=\"grid_3 iconabstand\">";
							echo "<div class=\"box wow fadeInRight\">";
								echo "<div class=\"box_aside active\">";
									echo "<div class=\"icon3\">";
										echo "<img class='svg' src='images/kacheln-einheit/einheit_weiss.svg' titel='Angaben zur Einheit' alt='Angaben zur Einheit' />";
										echo "<p>EINHEIT</p>";
										echo "<p></p>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</li>";
					echo "</a>";
					
					echo "<a href=\"\" class='js-step js-no-action' data-step='2'>";
						echo "<li class=\"grid_3 iconabstand\">";
							echo "<div class=\"box wow fadeInRight\">";
								echo "<div class=\"box_aside\">";
									echo "<div class=\"icon3\">";
										echo "<img class='svg' src='images/kacheln-einheit/urkunde_schwarz.svg' data-img='urkunde' titel='Angaben zu den Erwerbsnebenkosten' alt='Angaben zu den Erwerbsnebenkosten' />";
										echo "<p>ERWERBSNEBENKOSTEN</p>";
										echo "<p></p>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</li>";
					echo "</a>";

					echo "<a href=\"\" class='js-step js-no-action' data-step='3'>";
						echo "<li class=\"grid_3 iconabstand\">";
							echo "<div class=\"box wow fadeInRight\">";
								echo "<div class=\"box_aside\">";
									echo "<div class=\"icon3\">";
										echo "<img src='images/kacheln-einheit/miete_schwarz.svg' data-img='miete' titel='Angaben zu den Mieteinnahmen' alt='Angaben zu den Mieteinnahmen' />";
										echo "<p>MIETEINNAHMEN</p>";
										echo "<p></p>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</li>";
					echo "</a>";
					
					echo "<a href=\"\" class='js-step js-no-action' data-step='4'>";
						echo "<li class=\"grid_3 iconabstand\">";
							echo "<div class=\"box wow fadeInRight\">";
								echo "<div class=\"box_aside\">";
									echo "<div class=\"icon3\">";
										echo "<img src='images/kacheln-einheit/rechner_schwarz.svg' data-img='rechner' titel='Berechnen' alt='Berechnen' />";
										echo "<p>BERECHNEN</p>";
										echo "<p></p>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</li>";
					echo "</a>";
					
				echo "</ul>";
			echo "</div>";
		echo "</section>";
		
		//FORMULAR
		
		echo "<form class='immobilie-anlegen'>";
			
			/********* STEP 1 ***********/
			echo "<section class=\"well1 js-form-step\" id='form-step-1'>";
				echo "<div class=\"container-fluid\">";
					//Objektbild
						echo "<div class='grid_6'>";
							echo "<div class='header-blau'><p>Bild zur Einheit </p></div>";
							echo "<div id='bild-hochladen'>";
							?>
								<div class="slim ins7"
									 data-min-size="500,350"
									 data-size="500,350"
									 data-force-size="500,350"
									 data-post="output"
									 data-status-image-too-small="Ihr Bild muss mindestens eine Größe von 500 x 350px haben!"
									 data-label="<p id='upload-text'><br>Hier klicken oder per Drag and Drop <br>das Bild hier einfügen.</p>"
									 data-button-cancel-label="Abbrechen"
									 data-button-confirm-label="Übernehmen"
									 data-max-file-size="2"
									 data-save-initial-image="true">
									<input type="file" name="bild-upload"/>
								</div>
                            <?php
							echo "</div>";
						echo "</div>";
					/**/
					//Einheit Grunddaten
					echo "<div class='grid_6'>";
						echo "<div class='header-blau'><p>Einheit Grunddaten <span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						
						echo "<label for='einheitenbeschreibung'>Einheitenbeschreibung</label>";
						echo "<input type='text' name='einheitenbeschreibung' placeholder='Einheitenbeschreibung (z.B.: WE1 - H1)'/>";
						
						echo "<label for='einheitenlage'>Einheitenlage</label>";
						echo "<input type='text' name='einheitenlage' placeholder='Einheitenlage (z.B.: EG - RE)'/>";
						
						echo "<div class='input-strasse-hnr'>";
							echo "<label for='strasse'>Straße</label>";
							echo "<label for='hnr'>Hausnummer</label>";
							echo "<input type='text' name='strasse' placeholder='Straße'/>";
							echo "<input type='text' name='hnr' placeholder='HausNr'/>";
						echo "</div>";
						
						echo "<div class='input-plz-ort'>";
							echo "<label for='plz'>PLZ</label>";
							echo "<label for='ort'>Ort</label>";
							echo "<input type='text' name='plz' placeholder='PLZ'/>";
							echo "<input type='text' name='ort' placeholder='Ort'/>";
						echo "</div>";
						
						echo "<label for='typ'>Typ</label>";
						echo "<select id='typ' name='typ'>";
							echo "<option value=''>Typ wählen</option>";
							echo "<option value='wohnung'>Wohung</option>";
						echo "</select>";
					echo "</div>";
					/**/
					//Eckdaten
					echo "<div class='grid_12 well2 grunddaten'>";
						echo "<div class='header-blau'><p>Eckdaten <span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						
						
						echo "<div class='grid_4'>";
							echo "<label for='flaeche'>Fläche</label>";
								echo "<div class='qm'>";
									echo "<input type='text' name='flaeche' placeholder='0,00'>";
								echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='kaufpreis'>Kaufpreis</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='kaufpreis' placeholder='0,00'>";
								echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='kaupfreis_pro_qm'>Kaufpreis pro m<sup>2</sup></label>";
								echo "<div class='euro_qm'>";
									echo "<input type='text' name='kaupfreis_pro_qm' placeholder='0,00' readonly>";
								echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='kaufpreis_moebel'>Kaufpreis Möbel / Küche</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='kaufpreis_moebel' placeholder='0,00'>";
								echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='kaufpreis_aussenanlagen'>Kaufpreis Außenanlagen</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='kaufpreis_aussenanlagen' placeholder='0,00'>";
								echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='kosten_sanierung'>Kosten zusätzliche Sanierung / Renovierung</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='kosten_sanierung' placeholder='0,00'>";
								echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='baujahr'>Baujahr</label>";
							echo "<div>";
								echo "<select name='baujahr'>";
									echo "<option value=''>Baujahr wählen</option>";
									for($i = $jahr; $i >= 1800 ; $i--){
										echo "<option value='$i'>$i</option>";
									}
								echo "</select>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='fertigstellung'>Fertigstellung</label>";
							echo "<div>";
								echo "<input type='text' class='datepicker' name='fertigstellung' placeholder='Datum Fertigstellung'>";
								echo "<i class='fa-calendar js-calendar'></i>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='nutzungsbeginn'>Nutzungsbeginn</label>";
							echo "<div>";
								echo "<input type='text' class='datepicker' name='nutzungsbeginn' placeholder='Datum Nutzungsbeginn'>";
								echo "<i class='fa-calendar js-calendar'></i>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
					/**/	
					//Grunddaten Kaufpreisaufteilung
					echo "<div class='grid_12 grunddaten_kaufpries'>";
						echo "<div class='header-blau'><p>Grunddaten zur Kaufpreisaufteilung <span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_12'>";
							echo "<div class='grid_6'>";
								echo "<label for='objektart'>Objektart</label>";
								echo "<select id='objektart'>";
									echo "<option value=''>Objektart</option>";
									echo "<option value='1'>Neubau</option>";
									echo "<option value='2'>Altbau</option>";
									echo "<option value='3'>Denkmal</option>";
								echo "</select>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_12 grid_row' style='padding-top: 15px;'>";
							echo "<div></div>";
							echo "<div class='grid_6'>";
								echo "<label for='grund'>Grund & Boden</label>";
								echo "<div>";
									echo "<input type='text' name='grund' placeholder='Grund & Boden'/>";
								echo "</div>";
								echo "<div>";
									echo "<input type='text' name='grund_proz' placeholder='0,00'/>";
								echo "</div>";
								
								echo "<label for='altbausubstanz'>Altbausubstanz</label>";
								echo "<div>";
									echo "<input type='text' name='altbausubstanz' placeholder='Altbausubstanz'/>";
								echo "</div>";
								echo "<div>";
									echo "<input type='text' name='altbausubstanz_proz' placeholder='0,00'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_6'>";
								echo "<label for='neubau'>Herstellungskosten Neubau</label>";
								echo "<div>";
									echo "<input type='text' name='neubau' placeholder='Herstellungskosten'/>";
								echo "</div>";
								echo "<div>";
									echo "<input type='text' name='neubau_proz' placeholder='0,00'/>";
								echo "</div>";
								
								echo "<label for='sanierungskosten'>Sanierungskosten</label>";
								echo "<div>";
									echo "<input type='text' name='sanierungskosten' placeholder='Sanierungskosten'/>";
								echo "</div>";
								echo "<div>";
									echo "<input type='text' name='sanierungskosten_proz' placeholder='0,00'/>";
								echo "</div>";
							echo "</div>";
						echo "</div>";
						
					echo "</div>";
					/**/	
					
					//Zusätzliche Anschaffungen
					echo "<div class='grid_12 well2 grid_row erwerbsnebenkosten'>";
						echo "<div class='header-blau'><p>Zusätzliche Anschaf&shy;fungen Stellplatz / Carport / Garage / Tiefgarage <span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						
						
						echo "<div class='grid_4'>";
							echo "<label for='zusatz_art'>Objektart</label>";
							echo "<div>";
								echo "<select name='zusatz_art'>";
									echo "<option value=''>Objektart wählen</option>";
									echo "<option value='stellplatz'>Stellplatz</option>";
									echo "<option value='carport'>Carport</option>";
									echo "<option value='garage'>Garage</option>";
									echo "<option value='tiefgarage'>Tiefgarage</option>";
								echo "</select>";	
							echo "</div>";	
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='zusatz_preis'>Kaufpreis</label>";
							echo "<div class='euro'>";
								echo "<input type='text' name='zusatz_preis' placeholder='0,00'/>";
							echo "</div>";		
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='zusatz_afa'>Abschreibung des Kaufpreises</label>";
							echo "<div>";
								echo "<select name='zusatz_afa'>";
									echo "<option value=''>Abschreibung wählen</option>";
									echo "<option value='einheit'>AfA wie Einheit</option>";
								echo "</select>";	
							echo "</div>";	
						echo "</div>";
						
						echo "<input type='hidden' id='id' value='1' />";
						echo "<div id='addFields'></div>";
						
						echo "<div class='grid_11'>";
						echo "</div>";
						echo "<div class='grid_1'>";
							echo "<h3 class='gruen'>";
								echo "<a href='' class='js-add-form-fields'>";
									echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true'></i>";
								echo "</a>";
							echo "</h3>";
						echo "</div>";
						
						
					echo "</div>";
					/**/	
					
				echo "</div>";
			echo "</section>";
			
			/********* STEP 2 ***********/
			echo "<section class=\"well1 js-form-step\" id='form-step-2'>";
				echo "<div class=\"container-fluid\">";
					
					//Grunddaten Erwerbskosten
					echo "<div class='grid_12 well2 grid_row erwerbskosten'>";
						echo "<div class='header-blau'><p>Grunddaten zu den Erwerbskosten<span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						
						echo "<div class='grid_5'>";
							echo "<label for='erwerbskosten_bez'>Bezeichnung</label>";
							echo "<input type='text' name='erwerbskosten_bez' placeholder='Bezeichnung'/>";	
						echo "</div>";
						
						echo "<div class='grid_3'>";
							echo "<label for='erwerbskosten_typ'>Typ</label>";
							echo "<div>";
								echo "<select name='erwerbskosten_bez'>";
									echo "<option value=''>Typ wählen</option>";
									echo "<option value='werbungskosten'>Werbungskosten</option>";
								echo "</select>";	
							echo "</div>";	
						echo "</div>";
						
						echo "<div class='grid_2'>";
							echo "<label for='erwerbskosten_jahr'>Jahr</label>";
							echo "<input type='text' name='erwerbskosten_jahr' placeholder=''/>";	
						echo "</div>";
						
						echo "<div class='grid_2'>";
							echo "<label for='erwerbskosten_proz'>Prozent</label>";
							echo "<div class='proz'>";
								echo "<input type='text' name='erwerbskosten_proz' placeholder='0,00' value='100'/>";	
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_12 ins7 grid_row'>";
							echo "<div></div>";	
							echo "<div class='grid_3 tooltip'>";
								echo "<label for='erwerbskosten_bezugsgroesse'>Bezugsgröße</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='erwerbskosten_bezugsgroesse' placeholder='0,00' class='js-bezugsgroesse'/>";	
								echo "</div>";
								echo "<div class='informationen'>";
                                	echo "<i class='special fa-question-circle fa-2x'>";
                                    	echo "<div class='desc'>Webinare, die als Videofile hinterlegt sind und passend auf alle Endgeräte konvertiert werden. Dadurch sind diese immer und jederzeit abrufbar.</div>";
                                    echo "</i>";
                            	echo "</div>";	
							echo "</div>";
							echo "<div class='grid_3'>";
								echo "<label for='erwerbskosten_bezugsgroesse_proz'>Prozent</label>";
								echo "<div class='proz'>";
									echo "<input type='text' name='erwerbskosten_bezugsgroesse_proz' placeholder='0,00' class='js-bezugsgroesse-proz'/>";	
								echo "</div>";
							echo "</div>";
							echo "<div class='grid_3'>";
								echo "<label for='erwerbskosten_betrag'>Betrag</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='erwerbskosten_betrag' placeholder='0,00' class='js-bezugsgroesse-betrag' readonly/>";	
								echo "</div>";	
							echo "</div>";
						echo "</div>";
						
						echo "<input type='hidden' id='id_3' value='1' />";
						echo "<div id='addFields3'></div>";
						
						echo "<div class='grid_11'>";
						echo "</div>";
						echo "<div class='grid_1'>";
							echo "<h3 class='gruen'>";
								echo "<a href='' class='js-add-form-fields-3'>";
									echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true'></i>";
								echo "</a>";
							echo "</h3>";
						echo "</div>";
					echo "</div>";
					/**/	
					
					//Grunddaten laufende Nebenkosten
					echo "<div class='grid_12 grid_row lauf_nebenkosten'>";
						echo "<div class='header-blau'><p>Grunddaten zu den laufenden monatlichen Nebenkosten<span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_12 grid_row'>";
							echo "<div></div>";
							echo "<div class='grid_5'>";
								echo "<label for='lauf_nebenkosten_bezeichnung'>Bezeichnung</label>";
								echo "<div>";
									echo "<input type='text' name='lauf_nebenkosten_bezeichnung' placeholder='Bezeichnung'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_2'>";
								echo "<label for='lauf_nebenkosten_abzugsfaehig'>Steuerlich Abzugsfähig</label>";
								echo "<div>";
									echo "<select name='lauf_nebenkosten_abzugsfaehig'>";
										echo "<option value=''>Wählen</option>";
										echo "<option value='ja'>Ja</option>";
										echo "<option value='nein'>Nein</option>";
									echo "</select>";	
								echo "</div>";	
							echo "</div>";
							
							echo "<div class='grid_3'>";
								echo "<label for='lauf_nebenkosten_qm'>Standartwert in € oder €/m<sup>2</sup></label>";
								
								echo "<div class='einheit'>";
										echo "<input type='text' name='lauf_nebenkosten_qm' placeholder='0,00'/>";
										echo "<div class='select-wrapper'>";
											echo "<select name='lauf_nebenkosten_qm_einheit' id='select_qm'>";
												echo "<option value='euro'>€</option>";
												echo "<option value='euro_qm'>€/m<sup>2</sup></option>";
											echo "</select>";
										echo "</div>";
									echo "</div>";		
							echo "</div>";
							
							echo "<div class='grid_2'>";
								echo "<br>";
								echo "<button type='button' class='btn14 js-show-erweiterung show-erweiterung' >Erweiterung</button>";
							echo "</div>";
							
							echo "<div class='grid_12 ins7 grid_row js-erweiterung erweiterung'>";
								echo "<div></div>";
								echo "<div class='grid_3'>";
									echo "<label for='erweiterung_nebenkosten_summe'>Summe Standartwert</label>";
									echo "<div class='euro'>";
										echo "<input type='text' name='erweiterung_nebenkosten_summe' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";	
								echo "<div class='grid_3'>";
									echo "<label for='erweiterung_nebenkosten_steigerung_ab'>Steigerung ab Jahr</label>";
									echo "<select name='erweiterung_nebenkosten_steigerung_ab'>";
										for($i = 0; $i<= 10; $i++){
											echo "<option value='$i'>$i</option>";
										}
									echo "</select>";	
								echo "</div>";
								echo "<div class='grid_3'>";
									echo "<label for='erweiterung_nebenkosten_steigerung_alle'>Steigerung alle Jahr</label>";
									echo "<select name='erweiterung_nebenkosten_steigerung_alle'>";
										for($i = 0; $i<= 10; $i++){
											echo "<option value='$i'>$i</option>";
										}
									echo "</select>";	
								echo "</div>";
								echo "<div class='grid_3'>";
									echo "<label for='erweiterung_nebenkosten_steigerung'>Steigerung in € oder %</label>";
									
									echo "<div class='einheit'>";
										echo "<input type='text' name='erweiterung_nebenkosten_steigerung' placeholder='0,00'/>";
										echo "<div class='select-wrapper'>";
											echo "<select name='erweiterung_nebenkosten_steigerung_einheit'>";
												echo "<option value='euro'>€</option>";
												echo "<option value='prozent'>%</option>";
											echo "</select>";
										echo "</div>";
									echo "</div>";
									
								echo "</div>";
							echo "</div>";
						echo "</div>";
						
						echo "<input type='hidden' id='id_2' value='1' />";
						echo "<div id='addFields2'></div>";
						
						echo "<div class='grid_11'>";
						echo "</div>";
						echo "<div class='grid_1'>";
							echo "<h3 class='gruen'>";
								echo "<a href='' class='js-add-form-fields-2'>";
									echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true' ></i>";
								echo "</a>";
							echo "</h3>";
						echo "</div>";
						
					echo "</div>";
					/**/	
					
				echo "</div>";
			echo "</section>";
			
			/********* STEP 3 ***********/
			echo "<section class=\"well1 js-form-step active\" id='form-step-3'>";
				echo "<div class=\"container-fluid\">";
				
					//VARIABLEN AUS DB
					$vtext01 = 1000.00; //Gesamtmiete
					$vtext02 = '2019-04-01'; //Nutzungsbeginn
					$vtext03 = '20'; //Betrachtungszeitraum
					
					//VARIABLEN VERARBEITEN
					if($vtext01 != '') {
						$vtext01 = number_format($vtext01,2,',','.');
					} else {
						$vtext01 = '0,00';
					}
					
					$vtext04 = date('Y', strtotime($vtext02)); //Nutzungsbeginn Jahr
					$vtext05 = date('m', strtotime($vtext02)); //Nutzungsbeginn Monat
					
					echo "<div id='gesamtmiete_db' data-value='$vtext01'></div>";
					echo "<div id='nutzungsbeginn_monat_db' data-value='$vtext05'></div>";
					
					//Monatliche Miteinnahmen
					echo "<div class='grid_12 grid_row lauf_nebenkosten'>";
						echo "<div class='header-blau'><p>Monatliche Mieteinnahmen zur Einheit<span class='js-refresh' data-id='reload_miete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_12 grid_row' id='reload_miete'>";
							echo "<div></div>";
							echo "<div class='grid_4'>";
								echo "<label for='miete'>Miete in €/m<sup>2</sup></label>";
								echo "<div class='euro_qm'>";
									echo "<input type='text' name='miete' placeholder='0,00'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_4'>";
								echo "<label for='gesamtmiete'>Gesamtmiete in €</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='gesamtmiete' placeholder='0,00' value='$vtext01'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_1'></div>";
							
							echo "<div class='grid_3'>";
								echo "<br>";
								echo "<button type='button' class='btn14 js-show-erweiterung show-erweiterung js-set-table' >Erweiterung</button>";
							echo "</div>";
							
							echo "<div class='grid_12 ins7 grid_row js-erweiterung erweiterung'>";
								echo "<div></div>";
								echo "<div class='grid_2'>";
									echo "<label for='miete_steigerung_x'>Steigerung alle x Jahre</label>";
									echo "<div class=''>";
										echo "<input type='text' name='miete_steigerung_x' placeholder='0' value='0' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_2 mietsteigerung'>";
									echo "<label for='miete_steigerung_euro'>Steigerung in Euro</label>";
									echo "<div class='euro'>";
										echo "<input type='text' name='miete_steigerung_euro' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_1'>";
									echo "<p style='text-align: center;font-size: 120%;padding-top: 50px;'>ODER</p>";
								echo "</div>";
								echo "<div class='grid_2'>";
									echo "<label for='miete_steigerung_proz'>Steigerung in Proz.</label>";
									echo "<div class='proz'>";
										echo "<input type='text' name='miete_steigerung_proz' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_2'>";
									echo "<label for='mietausfall_proz'>Mietausfall in Prozent</label>";
									echo "<div class='proz'>";
										echo "<input type='text' name='mietausfall_proz' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								
								echo "<div class='grid_12 grid_row ins7'>";
									echo "<div></div>";
									echo "<div class='grid_3'>";
										echo "<label for='miete_von'>Änderung von</label>";
										echo "<select name='miete_von' data-select='0'>";
											for($i = $vtext04; $i<= $vtext04 + $vtext03; $i++){
												echo "<option value='$i'>$i</option>";
											}
										echo "</select>";	
									echo "</div>";
									echo "<div class='grid_3'>";
										echo "<label for='miete_bis'>Änderung bis</label>";
										echo "<select name='miete_bis' data-select='last'>";
											for($i = $vtext04; $i<= $vtext04 + $vtext03; $i++){
												echo "<option value='$i' ".(($vtext04 + $vtext03) == $i ? "selected" : "").">$i</option>";
											}
										echo "</select>";	
									echo "</div>";
									
									echo "<div class='grid_3'>";
										echo "<label for='miete_steigerung_monat'>Monat</label>";
										echo "<select name='miete_steigerung_monat' data-select='0'>";
												echo "<option value='01'>Januar</option>";
												echo "<option value='02'>Februar</option>";
												echo "<option value='03'>März</option>";
												echo "<option value='04'>April</option>";
												echo "<option value='05'>Mai</option>";
												echo "<option value='06'>Juni</option>";
												echo "<option value='07'>Juli</option>";
												echo "<option value='08'>August</option>";
												echo "<option value='08'>September</option>";
												echo "<option value='10'>Oktober</option>";
												echo "<option value='11'>November</option>";
												echo "<option value='12'>Dezember</option>";
										echo "</select>";	
									echo "</div>";
									
									echo "<div class='grid_3'>";
										echo "<br>";
										echo "<button type='button' class='btn14 js-fill-table' >Werte setzen</button>";	
									echo "</div>";
								echo "</div>";/*ENDE grid_12*/
								
								echo "<div class='grid_12 well2'>";
									echo "<table class='js-resp-table'>";
										echo "<thead>";
											echo "<tr>";
												echo "<th>";
													echo "Jahr";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jan.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Feb.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Mär.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Apr.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Mai";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jun.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jul.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Aug.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Sep.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Okt.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Nov.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Dez.";
												echo "</th>";
												echo "<th  data-breakpoints='xs'>";
												echo "</th>";
											echo "</tr>";
										echo "</thead>";
										
										echo "<tbody>";
											$jahr = $vtext04;
											for($i = 1; $i <= $vtext03 + 1; $i++)
											{
												echo "<tr data-jahr='$jahr'>";
													echo "<td>";
														echo $jahr;
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jan_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_feb_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_maerz_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_apr_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_mai_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jun_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jul_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_aug_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_sep_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_okt_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_nov_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_dez_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<button type='button' class='btn14 js-clear-tr'>leeren</button>";
													echo "</td>";
												echo "</tr>";
												
												$jahr = $jahr + 1;
											}
										echo "</tbody>";
									echo "</table>";
								echo "</div>";/*ENDE grid_12 table*/
								
							echo "</div>";/*ENDE grid_12 show_erweiterung*/
							
						echo "</div>";/*ENDE grid_12*/
					echo "</div>";
					/**/	
					
					//Daten aus Datenbank mit Schleife
					//VARIABLEN AUS DB
					$vtext06 = 'Möbel / Küche'; //Name Objektart
					$vtext07 = 50.00; //Gesamtmiete
					
					//VARIABLEN VERARBEITEN
					if($vtext07 != '') {
						$vtext07 = number_format($vtext07,2,',','.');
					} else {
						$vtext07 = '0,00';
					}
					
					echo "<div class='grid_12 grid_row lauf_nebenkosten well2'>";
						echo "<div class='header-blau'><p>Monatliche Mieteinnahmen zu $vtext06<span class='js-refresh big'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_12 grid_row' id='reload'>";
							echo "<div></div>";
							echo "<div class='grid_4'>";
								echo "<label for='miete'>Miete in €/m<sup>2</sup></label>";
								echo "<div class='euro_qm'>";
									echo "<input type='text' name='miete' placeholder='0,00'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_4'>";
								echo "<label for='gesamtmiete'>Gesamtmiete in €</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='gesamtmiete' placeholder='0,00' value='$vtext07'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_1'></div>";
							
							echo "<div class='grid_3'>";
								echo "<br>";
								echo "<button type='button' class='btn14 js-show-erweiterung show-erweiterung js-set-table' >Erweiterung</button>";
							echo "</div>";
							
							echo "<div class='grid_12 ins7 grid_row js-erweiterung erweiterung'>";
								echo "<div></div>";
								echo "<div class='grid_2'>";
									echo "<label for='miete_steigerung_x'>Steigerung alle x Jahre</label>";
									echo "<div class=''>";
										echo "<input type='text' name='miete_steigerung_x' placeholder='0' value='0' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_2 mietsteigerung'>";
									echo "<label for='miete_steigerung_euro'>Steigerung in Euro</label>";
									echo "<div class='euro'>";
										echo "<input type='text' name='miete_steigerung_euro' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_1'>";
									echo "<p style='text-align: center;font-size: 120%;padding-top: 50px;'>ODER</p>";
								echo "</div>";
								echo "<div class='grid_2'>";
									echo "<label for='miete_steigerung_proz'>Steigerung in Proz.</label>";
									echo "<div class='proz'>";
										echo "<input type='text' name='miete_steigerung_proz' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_2'>";
									echo "<label for='mietausfall_proz'>Mietausfall in Prozent</label>";
									echo "<div class='proz'>";
										echo "<input type='text' name='mietausfall_proz' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								
								echo "<div class='grid_12 grid_row ins7'>";
									echo "<div></div>";
									echo "<div class='grid_3'>";
										echo "<label for='miete_von'>Änderung von</label>";
										echo "<select name='miete_von' data-select='0'>";
											for($i = $vtext04; $i<= $vtext04 + $vtext03; $i++){
												echo "<option value='$i'>$i</option>";
											}
										echo "</select>";	
									echo "</div>";
									echo "<div class='grid_3'>";
										echo "<label for='miete_bis'>Änderung bis</label>";
										echo "<select name='miete_bis' data-select='last'>";
											for($i = $vtext04; $i<= $vtext04 + $vtext03; $i++){
												echo "<option value='$i' ".(($vtext04 + $vtext03) == $i ? "selected" : "").">$i</option>";
											}
										echo "</select>";	
									echo "</div>";
									
									echo "<div class='grid_3'>";
										echo "<label for='miete_steigerung_monat'>Monat</label>";
										echo "<select name='miete_steigerung_monat' data-select='0'>";
												echo "<option value='01'>Januar</option>";
												echo "<option value='02'>Februar</option>";
												echo "<option value='03'>März</option>";
												echo "<option value='04'>April</option>";
												echo "<option value='05'>Mai</option>";
												echo "<option value='06'>Juni</option>";
												echo "<option value='07'>Juli</option>";
												echo "<option value='08'>August</option>";
												echo "<option value='08'>September</option>";
												echo "<option value='10'>Oktober</option>";
												echo "<option value='11'>November</option>";
												echo "<option value='12'>Dezember</option>";
										echo "</select>";	
									echo "</div>";
									
									echo "<div class='grid_3'>";
										echo "<br>";
										echo "<button type='button' class='btn14 js-fill-table' >Werte setzen</button>";	
									echo "</div>";
								echo "</div>";/*ENDE grid_12*/
								
								echo "<div class='grid_12 well2'>";
									echo "<table class='js-resp-table'>";
										echo "<thead>";
											echo "<tr>";
												echo "<th>";
													echo "Jahr";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jan.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Feb.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Mär.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Apr.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Mai";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jun.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jul.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Aug.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Sep.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Okt.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Nov.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Dez.";
												echo "</th>";
												echo "<th  data-breakpoints='xs'>";
												echo "</th>";
											echo "</tr>";
										echo "</thead>";
										echo "<tbody>";
											$jahr = $vtext04;
											for($i = 1; $i <= $vtext03 + 1; $i++)
											{
												echo "<tr data-jahr='$jahr'>";
													echo "<td>";
														echo $jahr;
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jan_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_feb_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_maerz_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_apr_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_mai_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jun_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jul_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_aug_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_sep_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_okt_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_nov_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_dez_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<button type='button' class='btn14 js-clear-tr'>leeren</button>";
													echo "</td>";
												echo "</tr>";
												
												$jahr = $jahr + 1;
											}
										echo "</tbody>";
									echo "</table>";
								echo "</div>";/*ENDE grid_12 table*/
								
							echo "</div>";/*ENDE grid_12 show_erweiterung*/
							
						echo "</div>";/*ENDE grid_12*/
					echo "</div>";
					/**/
					
					//Monatliche Miteinnahmen INDIVIDUELL
					echo "<div class='grid_12'>";
						echo "<div class='grid_11'>";
						echo "</div>";
						echo "<div class='grid_1'>";
							echo "<h3 class='gruen'>";
								echo "<a href='' class='js-show-erweiterung-plus'>";
									echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true' ></i>";
								echo "</a>";
							echo "</h3>";
						echo "</div>";
					echo "</div>";
						
					echo "<div class='grid_12 grid_row lauf_nebenkosten erweiterung-plus ins7'>";
						echo "<div class='header-blau'><p>Sonstige monatliche Mieteinnahmen<span class='js-refresh'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_12'>";
							echo "<div class='grid_4'>";
								echo "<label for='miete_individuell_name'>Beschreibung</label>";
								echo "<div class=''>";
									echo "<input type='text' name='miete_individuell_name' placeholder='Beschreibung'/>";
								echo "</div>";
							echo "</div>";
						echo "</div>";
							
						echo "<div class='grid_12 grid_row ins7' id='reload'>";
							echo "<div></div>";
							echo "<div class='grid_4'>";
								echo "<label for='miete'>Miete in €/m<sup>2</sup></label>";
								echo "<div class='euro_qm'>";
									echo "<input type='text' name='miete' placeholder='0,00'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_4'>";
								echo "<label for='gesamtmiete'>Gesamtmiete in €</label>";
								echo "<div class='euro'>";
									echo "<input type='text' name='gesamtmiete' placeholder='0,00' />";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_1'></div>";
							
							echo "<div class='grid_3'>";
								echo "<br>";
								echo "<button type='button' class='btn14 js-show-erweiterung show-erweiterung js-set-table' >Erweiterung</button>";
							echo "</div>";
							
							echo "<div class='grid_12 ins7 grid_row js-erweiterung erweiterung'>";
								echo "<div></div>";
								echo "<div class='grid_2'>";
									echo "<label for='miete_steigerung_x'>Steigerung alle x Jahre</label>";
									echo "<div class=''>";
										echo "<input type='text' name='miete_steigerung_x' placeholder='0' value='0' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_2 mietsteigerung'>";
									echo "<label for='miete_steigerung_euro'>Steigerung in Euro</label>";
									echo "<div class='euro'>";
										echo "<input type='text' name='miete_steigerung_euro' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_1'>";
									echo "<p style='text-align: center;font-size: 120%;padding-top: 50px;'>ODER</p>";
								echo "</div>";
								echo "<div class='grid_2'>";
									echo "<label for='miete_steigerung_proz'>Steigerung in Proz.</label>";
									echo "<div class='proz'>";
										echo "<input type='text' name='miete_steigerung_proz' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								echo "<div class='grid_2'>";
									echo "<label for='mietausfall_proz'>Mietausfall in Prozent</label>";
									echo "<div class='proz'>";
										echo "<input type='text' name='mietausfall_proz' placeholder='0,00' />";	
									echo "</div>";	
								echo "</div>";
								
								echo "<div class='grid_12 grid_row ins7'>";
									echo "<div></div>";
									echo "<div class='grid_3'>";
										echo "<label for='miete_von'>Änderung von</label>";
										echo "<select name='miete_von' data-select='0'>";
											for($i = $vtext04; $i<= $vtext04 + $vtext03; $i++){
												echo "<option value='$i'>$i</option>";
											}
										echo "</select>";	
									echo "</div>";
									echo "<div class='grid_3'>";
										echo "<label for='miete_bis'>Änderung bis</label>";
										echo "<select name='miete_bis' data-select='last'>";
											for($i = $vtext04; $i<= $vtext04 + $vtext03; $i++){
												echo "<option value='$i' ".(($vtext04 + $vtext03) == $i ? "selected" : "").">$i</option>";
											}
										echo "</select>";	
									echo "</div>";
									
									echo "<div class='grid_3'>";
										echo "<label for='miete_steigerung_monat'>Monat</label>";
										echo "<select name='miete_steigerung_monat' data-select='0'>";
												echo "<option value='01'>Januar</option>";
												echo "<option value='02'>Februar</option>";
												echo "<option value='03'>März</option>";
												echo "<option value='04'>April</option>";
												echo "<option value='05'>Mai</option>";
												echo "<option value='06'>Juni</option>";
												echo "<option value='07'>Juli</option>";
												echo "<option value='08'>August</option>";
												echo "<option value='08'>September</option>";
												echo "<option value='10'>Oktober</option>";
												echo "<option value='11'>November</option>";
												echo "<option value='12'>Dezember</option>";
										echo "</select>";	
									echo "</div>";
									
									echo "<div class='grid_3'>";
										echo "<br>";
										echo "<button type='button' class='btn14 js-fill-table' >Werte setzen</button>";	
									echo "</div>";
								echo "</div>";/*ENDE grid_12*/
								
								echo "<div class='grid_12 well2'>";
									echo "<table class='js-resp-table'>";
										echo "<thead>";
											echo "<tr>";
												echo "<th>";
													echo "Jahr";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jan.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Feb.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Mär.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Apr.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Mai";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jun.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Jul.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Aug.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Sep.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Okt.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Nov.";
												echo "</th>";
												echo "<th data-breakpoints='xs'>";
													echo "Dez.";
												echo "</th>";
												echo "<th  data-breakpoints='xs'>";
												echo "</th>";
											echo "</tr>";
										echo "</thead>";
										
										echo "<tbody>";
											$jahr = $vtext04;
											for($i = 1; $i <= $vtext03 + 1; $i++)
											{
												echo "<tr data-jahr='$jahr'>";
													echo "<td>";
														echo $jahr;
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jan_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_feb_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_maerz_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_apr_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_mai_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jun_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_jul_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_aug_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_sep_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_okt_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_nov_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<input type='text' name='miete_dez_$jahr' value='0,00'>";
													echo "</td>";
													echo "<td>";
														echo "<button type='button' class='btn14 js-clear-tr'>leeren</button>";
													echo "</td>";
												echo "</tr>";
												
												$jahr = $jahr + 1;
											}
										echo "</tbody>";
									echo "</table>";
								echo "</div>";/*ENDE grid_12 table*/
								
							echo "</div>";/*ENDE grid_12 show_erweiterung*/
							
						echo "</div>";/*ENDE grid_12*/
					echo "</div>";
					/**/		
					
				echo "</div>";
			echo "</section>";
			
			/********* STEP 4 ***********/
			echo "<section class=\"well1 js-form-step\" id='form-step-4'>";
				echo "<div class=\"container-fluid\">";
				echo "</div>";
			echo "</section>";
			
		echo "</form>";
		
		//FORMULAR ENDE


	?>
		
      </main>
      
<script> 
	
</script>   
     
      
<!-- ==============FOOTER============== -->
                      
<?php      
 include ("inc/end.php");
?>