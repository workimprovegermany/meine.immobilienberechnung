<?php
define("_BASE_","immobilien.php");  // filename (basename perl&php);
$stamp = filemtime(_BASE_);
$last_touch =  date("d.m.Y", $stamp);
$stunde  = date("H");
function utime ()
{
$time = explode( " ", microtime());
$usec = (double)$time[0];
$sec = (double)$time[1];
return $sec + $usec;
}
$start = utime();
$stamp = time();
$heute = gmdate("d m Y H:i:s" , $stamp);
$tag  = date("d");
$monat  = date("m");
$jahr  = date("Y");
$uhr  = date("G");
$minute  = date("i");
$datum = $tag.".".$monat.".".$jahr;
$datum_01 = $jahr."-".$monat."-".$tag;


require("global_funcs.php");
require("inc/lib_incl_intern.php");
?>

<?php

/*
if (!defined('_IS_VALID_')  || $auth->prio <= "0")
{
  echo "Unerlaubter Zugriff.....";
  echo "</body>";
  echo "</html>";
  exit();
}

else
{*/
include "inc/head.inc.php";
include "inc/header.php";

// Hier wird die Nav nach der Prio  aus gegeben ! 
include "nav/nav.php";

//}


  

?>


<!-- ==============CONTENT============== -->
  
      <main>


<?php

		echo "<section class=\"well16 suche\">";
			echo "<div class=\"container-fluid\">";
				echo "<div class='grid_4'>";
					echo "<input type='text' name='search-text' />";
				echo "</div>";
				echo "<div class='grid_4'>";
					echo "<select>";
						echo "<option value=''>Objektart</option>";
						echo "<option value='1'>Neubau</option>";
						echo "<option value='2'>Altbau</option>";
						echo "<option value='3'>Denkmal</option>";
					echo "</select>";
				echo "</div>";
				echo "<div class='grid_4'>";
					echo "<button type='button' class='btn14'><img src='images/lupe.svg' /> Suchen </button>";
				echo "</div>";
			echo "</div>";
		echo "</section>";
		
		echo "<section class=\"well16\">";
			echo "<div class=\"container-fluid\">";
				echo "<div class='grid_11'></div>";
				echo "<div class='grid_1'>";
					echo "<h3 class='gruen'>";
						echo "<a href='immobilien-anlegen.php'>";
							echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true' title='Neue Immobilie anlegen'></i>";
						echo "</a>";
					echo "</h3>";
				echo "</div>";
			echo "</div>";
		echo "</section>";
			
		echo "<section class=\"well1\">";
			echo "<div class=\"container-fluid\">";
			
				//PHP-Schleife hier starten
				//Variablen
				$vtext01 = 'denkmal'; 
				$vtext02 = 'images/test/immobilie_1.jpg'; //Pfad Bild von Immobilie
				$vtext03 = 'Denkmalobjekt'; //Objektart
				$vtext04 = 'Sachsen'; //Bundesland
				$vtext05 = number_format('126000', 2, ',', '.') ." €";//Kaufpreis
				$vtext06 = '1950'; //Baujahr
				$vtext07 = 'Ludwig von Annaberg Str. 17'; //Straße
				$vtext08 = 'Leipzig'; //Ort
				$vtext09 = 14; //frei
				$vtext10 = 4; //reserviert
				$vtext11 = 5; //verkauft
				$vtext12 = 1; //ID
				$vtext13 = 1; //ID ob Bauträger oder Mensch (Bsp. 1 = Bauträger)
				$vtext14 = 'Das Baudenkmal GmbH'; //Name Bauträger oder Mensch
				$vtext15 = 0; //ID für Immobiliendetails anschauen 
				
				echo "<div class='immobilie'>";
					echo "<div class='grid_4 bild-container'>";
						echo "<div class='$vtext01' style='background-image: url($vtext02)'>";
							echo "<img src='$vtext02' />";
						echo "</div>";
					echo "</div>";
					
					echo "<div class='grid_8 info-container'>";
					
						echo "<div class='grid_4 info'>";
							echo "<img class='icon_haus' src='images/haus.svg' />";
							echo "<div>";
								echo "<p>$vtext03</p>";
								echo "<p>$vtext04</p>";
							echo "</div>";
						echo "</div>";	
						
						echo "<div class='grid_4 info'>";
							echo "<div>";
								echo "<p>Kaufpreis: <span>ab $vtext05</span></p>";
								echo "<p>Baujahr: <span>$vtext06</span></p>";
							echo "</div>";
						echo "</div>";
							
						echo "<div class='grid_4 info ".(($vtext13 == 1) ? "bautraeger" : "")."'>";
							echo "<div></div>";
							echo "<p>$vtext14</p>";	
						echo "</div>";
						
						echo "<div class='grid_4 info'>";
							echo "<p>$vtext07</p>";
							echo "<p><img class='icon_ort' src='images/standort.svg' /> <strong>$vtext08</strong></p>";
						echo "</div>";
							
						echo "<div class='grid_4 info'>";
							echo "<div>";
								echo "<p>Einheiten:</p>";
							echo "</div>";
							echo "<div>";
								echo "<p>".(($vtext09 < 10) ? "&nbsp; " : "")."$vtext09 &nbsp;&nbsp;<span class='kreis_gruen'></span>&nbsp;&nbsp;FREI&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>";
								echo "<p>".(($vtext10 < 10) ? "&nbsp; " : "")."$vtext10 &nbsp;&nbsp;<span class='kreis_gelb'></span>&nbsp;&nbsp;RESERVIERT</p>";
								echo "<p>".(($vtext11 < 10) ? "&nbsp; " : "")."$vtext11 &nbsp;&nbsp;<span class='kreis_rot'></span>&nbsp;&nbsp;VERKAUFT&nbsp; &nbsp;</p>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4 info'>";
							echo "<a class='".(($vtext15 == 1) ? "active " : "")."' href='immobilien-detail.php?id=$vtext12'><i class='fa-sign-in'></i></a>";
						echo "</div>";
							
					echo "</div>"; //ENDE grid_8
				echo "</div>";
				
				//PHP-Schleife hier beenden	
				
				//LÖSCHEN WENN SCHLEIFE FERTIG!!!!!!!!!!
				//Variablen
				$vtext01 = 'neubau'; 
				$vtext02 = 'images/test/immobilie_2.png'; //Pfad Bild von Immobilie
				$vtext03 = 'Neubau'; //Objektart
				$vtext04 = 'Bayern'; //Bundesland
				$vtext05 = number_format('213000', 2, ',', '.') ." €";//Kaufpreis
				$vtext06 = '2019'; //Baujahr
				$vtext07 = 'Landsberger Allee 294'; //Straße
				$vtext08 = 'München'; //Ort
				$vtext09 = 14; //frei
				$vtext10 = 4; //reserviert
				$vtext11 = 5; //verkauft
				$vtext12 = 1; //ID
				$vtext13 = 2; //ID ob Bauträger oder Mensch (Bsp. 1 = Bauträger)
				$vtext14 = 'Klaus von Herrenberg KG'; //Name Bauträger oder Mensch
				$vtext15 = 1; //ID für Immobiliendetails anschauen 
				
				echo "<div class='immobilie well16'>";
					echo "<div class='grid_4 bild-container'>";
						echo "<div class='$vtext01' style='background-image: url($vtext02)'>";
							echo "<img src='$vtext02' />";
						echo "</div>";
					echo "</div>";
					
					echo "<div class='grid_8 info-container'>";
					
						echo "<div class='grid_4 info'>";
							echo "<img class='icon_haus' src='images/haus.svg' />";
							echo "<div>";
								echo "<p>$vtext03</p>";
								echo "<p>$vtext04</p>";
							echo "</div>";
						echo "</div>";	
						
						echo "<div class='grid_4 info'>";
							echo "<div>";
								echo "<p>Kaufpreis: <span>ab $vtext05</span></p>";
								echo "<p>Baujahr: <span>$vtext06</span></p>";
							echo "</div>";
						echo "</div>";
							
						echo "<div class='grid_4 info ".(($vtext13 == 1) ? "bautraeger" : "")."'>";
							echo "<div></div>";
							echo "<p>$vtext14</p>";	
						echo "</div>";
						
						echo "<div class='grid_4 info'>";
							echo "<p>$vtext07</p>";
							echo "<p><img class='icon_ort' src='images/standort.svg' /> <strong>$vtext08</strong></p>";
						echo "</div>";
							
						echo "<div class='grid_4 info'>";
							echo "<div>";
								echo "<p>Einheiten:</p>";
							echo "</div>";
							echo "<div>";
								echo "<p>".(($vtext09 < 10) ? "&nbsp; " : "")."$vtext09 &nbsp;&nbsp;<span class='kreis_gruen'></span>&nbsp;&nbsp;FREI&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>";
								echo "<p>".(($vtext10 < 10) ? "&nbsp; " : "")."$vtext10 &nbsp;&nbsp;<span class='kreis_gelb'></span>&nbsp;&nbsp;RESERVIERT</p>";
								echo "<p>".(($vtext11 < 10) ? "&nbsp; " : "")."$vtext11 &nbsp;&nbsp;<span class='kreis_rot'></span>&nbsp;&nbsp;VERKAUFT&nbsp; &nbsp;</p>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4 info'>";
							echo "<a class='".(($vtext15 == 1) ? "active " : "")."' href='immobilien-detail.php?id=$vtext12'><i class='fa-sign-in'></i></a>";
						echo "</div>";
							
					echo "</div>"; //ENDE grid_8
				echo "</div>";
				/*ENDE*/
				
			echo "</div>";
		echo "</section>";

          ?>

      </main>
      
      
      
<!-- ==============FOOTER============== -->
                      
<?php      
 include ("inc/end.php");
?>