<?php

function select_online_id($value) 
{
  global $db_sets;
  global $global_tbl_100;
  $chk = $db_sets->escape_smart($value);
  $get = $db_sets->query("SELECT session_id FROM $global_tbl_100 WHERE session_id='$chk'");
  $num = $db_sets->num_rows($get);
  return $num;
}

function select_time_from_online($value) 
{
  global $db_sets;
  global $global_tbl_100;
  $get = $db_sets->query("SELECT id FROM $global_tbl_100 WHERE (UNIX_TIMESTAMP(NOW()) - $value) > UNIX_TIMESTAMP(start)");
  $res = $db_sets->fetch_array($get);
  return $res[0];
}


function get_my_last_session_time($v1,$v2)
{
  global $db_sets;
  global $global_tbl_100;
  $get = $db_sets->query("SELECT UNIX_TIMESTAMP(start) FROM $global_tbl_100 WHERE ip='$v2' AND session_id='$v1'");
  $res = $db_sets->fetch_array($get);
  return $res[0];
}
	

// Check Functions

function check_if_base_already_exists($value)
{
 global $db_sets;
 global $global_tbl_208;
 $get = $db_sets->query("SELECT id FROM $global_tbl_208 WHERE path='$value'");
 $num = $db_sets->num_rows($get);
 return $num;
}	

// Insert Functions

function insert_online_session_id($v1,$v2,$v3,$v4) 
{
  global $db_sets;
  global $global_tbl_100;
  $add = $db_sets->query("INSERT INTO $global_tbl_100 (`session_id`,`ip`,`path`,`entrydate`) VALUES ('$v1','$v2','$v3','$v4')");
}

function insert_into_global_counter($v1,$v2)
{
  global $db_sets;
  global $global_tbl_208;
  $add = $db_sets->query("INSERT INTO $global_tbl_208 (path,clicks,all_time) VALUES ('$v1','1','$v2')");
}	

// Update Functions


function update_online_session_id($v1,$v2,$v3,$v4)
{
	global $db_sets;
  global $global_tbl_100;
	$up1 = $db_sets->query("UPDATE $global_tbl_100 SET `entrydate`='$v4' WHERE `ip`='$v2' AND `session_id`='$v1'");
	$up2 = $db_sets->query("UPDATE $global_tbl_100 SET `path`='$v3'      WHERE `ip`='$v2' AND `session_id`='$v1'");
}

function update_global_counter($v1,$v2) 
{
 global $db_sets;
 global $global_tbl_208;
 $up1 = $db_sets->query("UPDATE $global_tbl_208 SET `clicks`=`clicks`+1         WHERE `path`='$v1'");
 $up2 = $db_sets->query("UPDATE $global_tbl_208 SET `all_time`=`all_time`+'$v2' WHERE `path`='$v1'");
}

// Delete Functions


function remove_online_session_id($v1) 
{
  global $db_sets;
  global $global_tbl_100;
  $del = $db_sets->query("DELETE FROM $global_tbl_100 WHERE `session_id`='$v1'");
}

function delete_id_from_online($v1) 
{
  global $db_sets;
  global $global_tbl_100;
  $del = $db_sets->query("DELETE FROM $global_tbl_100 WHERE `id`='$v1'");
}


?>