<?php

function get_name_after_login($value) 
{
   global $db_sets;
   global $global_tbl_100;
   $get = $db_sets->query("SELECT `uname`,`lang` FROM $global_tbl_100 WHERE `session_id`='$value' AND `loggedin`='1' LIMIT 1");
   $res = $db_sets->fetch_array($get);
   return $res;
}



function get_guest_name_after_login($v1) 
{
   global $db_sets;
   global $global_tbl_100;
   $get = $db_sets->query("SELECT `lang` FROM $global_tbl_100 WHERE `session_id`='$v1' AND `loggedin`='0' AND `uname`='guest' LIMIT 1");
   $res = $db_sets->fetch_array($get);
   return $res;
}

function get_status_auth_data($v1) 
{
  global $db_sets;
  global $global_tbl_200;
  $get = $db_sets->query("SELECT * FROM $global_tbl_200 WHERE `uid`='$v1' LIMIT 1");
  return $get;
}

function get_more_auth_email_data($v1) 
{
  global $db_sets;
  global $global_tbl_206;
  $get = $db_sets->query("SELECT `email` FROM $global_tbl_206 WHERE `uid`='$v1' LIMIT 1");
  return $get;
}

function get_more_auth_data($v1)  // tbl_user_name
{
  global $db_sets;
  global $global_tbl_210;
  $get = $db_sets->query("SELECT `anrede`, `name`,`vname` FROM $global_tbl_210 WHERE `uid`='$v1' LIMIT 1");
  return $get;
}


function get_auth_data($v1)  // tbl_user_login
{
  global $db_sets;
  global $global_tbl_208;
  $get = $db_sets->query("SELECT * FROM $global_tbl_208 WHERE `uname`='$v1' LIMIT 1");
  return $get;
}

function get_auth_user_aktuell($v1) 
{
  global $db_sets;
  global $global_tbl_201;
  $get = $db_sets->query("SELECT * FROM $global_tbl_201 WHERE `uid`='$v1' LIMIT 1");
  return $get;
}

function get_auth_user_firma($v1)
{
  global $db_sets;
  global $global_tbl_204;
  $get = $db_sets->query("SELECT * FROM $global_tbl_204 WHERE `uid`='$v1' LIMIT 1");
  return $get;
}




?>