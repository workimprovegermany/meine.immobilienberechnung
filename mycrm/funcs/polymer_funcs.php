<?php

// $global_tbl_216 = $prefix . "user_video";

		// ================== GET FUNCS ============= //

	  function get_admin_alle_preispakete($v1) // Werte aus Tabelle admin_preispakete 
	  {
		global $db_sets;
		global $global_tbl_27;
		$get = $db_sets->query("SELECT * FROM $global_tbl_27 WHERE `aktiv`=1 ORDER BY `id` ASC ");
		return $get;
	  }	

	  function get_admin_preispakete_nach_id($v1) // Werte aus Tabelle admin_preispakete 
	  {
		global $db_sets;
		global $global_tbl_27;
		$get = $db_sets->query("SELECT * FROM $global_tbl_27 WHERE `id`='$v1' AND `aktiv`=1 LIMIT 1 ");
		return $get;
	  }	

	  function get_admin_preispakete_01($v1) // Werte aus Tabelle admin_preispakete 
	  {
		global $db_sets;
		global $global_tbl_27;
		$get = $db_sets->query("SELECT * FROM $global_tbl_27 WHERE `k_id`='$v1' AND `aktiv`=1 ORDER BY `id` ");
		return $get;
	  }	


	  function get_admin_alle_preispakete_nach_beschreibung($v1) // Werte aus Tabelle admin_fahrzeuge  LIKE '$v1%' 
	  {
		global $db_sets;
		global $global_tbl_27;
		$get = $db_sets->query("SELECT * FROM $global_tbl_27 WHERE `beschreibung` LIKE '%$v1%' AND `aktiv`=1 ORDER BY `id` ASC ");
		return $get;
	  }	





		function get_admin_kunden_kategorie()  // tbl - kunde_kategorie
		{
		  global $db_sets;
		  global $global_tbl_50;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_50 ORDER BY `id` DESC ");
		  return $get;
		}
		function get_admin_kunden_kategorie_001($v1)  // tbl - kunde_kategorie
		{
		  global $db_sets;
		  global $global_tbl_50;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_50 WHERE `id` = '$v1' LIMIT 1 ");
		  return $get;
		}

		function get_admin_kunden_anrede($v1)  // tbl - kunde_anrede
		{
		  global $db_sets;
		  global $global_tbl_51;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_51 WHERE `id` = '$v1' LIMIT 1");
		  return $get;
		}

		function get_admin_kunden_anrede_alle_herr($v1,$v2)  // tbl - kunde_anrede
		{
		  global $db_sets;
		  global $global_tbl_51;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_51 WHERE `id` <= '2' ");
		  return $get;
		}

		function get_admin_alle_fahrzeuge()  // tbl - admin_fahrzeuge
		{
		  global $db_sets;
		  global $global_tbl_70;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_70 ORDER BY `id` ");
		  return $get;
		}
		
		function get_admin_letzte_10_fahrzeuge()  // tbl - admin_fahrzeuge
		{
		  global $db_sets;
		  global $global_tbl_70;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_70 ORDER BY `last_stamp` DESC LIMIT 10");
		  return $get;
		}
		
		
		
		
	  function get_admin_alle_fahrzeuge_nach_name($v1) // Werte aus Tabelle admin_fahrzeuge  LIKE '$v1%' 
	  {
		global $db_sets;
		global $global_tbl_70;
		$get = $db_sets->query("SELECT * FROM $global_tbl_70 WHERE `fahrzeug` LIKE '$v1%' ORDER BY `id` ASC ");
		return $get;
	  }	
		
	  function get_admin_alle_fahrzeuge_nach_id($v1) // Werte aus Tabelle admin_fahrzeuge  LIKE '$v1%' 
	  {
		global $db_sets;
		global $global_tbl_70;
		$get = $db_sets->query("SELECT * FROM $global_tbl_70 WHERE `id`='$v1' LIMIT 1 ");
		return $get;
	  }	
		
		function get_admin_alle_fahrzeuge_art()  // tbl - admin_fahrzeug_art
		{
		  global $db_sets;
		  global $global_tbl_71;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_71 ORDER BY `art_id` ");
		  return $get;
		}
		
		function get_admin_alle_fahrzeuge_art_nach_id($v1)  // tbl - admin_fahrzeug_art
		{
		  global $db_sets;
		  global $global_tbl_71;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_71 WHERE `art_id`='$v1' LIMIT 1 ");
		  return $get;
		}
		
		function get_admin_alle_fahrzeug_farbe()  // tbl - admin_fahrzeug_farbe
		{
		  global $db_sets;
		  global $global_tbl_72;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_72 ORDER BY `id` ");
		  return $get;
		}
		
		function get_admin_fahrzeug_farbe_nach_id($v1)  // tbl - admin_fahrzeug_farbe
		{
		  global $db_sets;
		  global $global_tbl_72;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_72 WHERE `id`='$v1' LIMIT 1 ");
		  return $get;
		}
		
		function get_admin_alle_fahrzeug_innen()  // tbl - admin_fahrzeug_innen
		{
		  global $db_sets;
		  global $global_tbl_73;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_73 ORDER BY `id` ");
		  return $get;
		}
		
		function get_admin_alle_fahrzeug_innen_nach_id($v1)  // tbl - admin_fahrzeug_innen
		{
		  global $db_sets;
		  global $global_tbl_73;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_73 WHERE `id`='$v1' ");
		  return $get;
		}
		
		function get_admin_alle_reinigung_typ()  // tbl - admin_fahrzeug_reinigung
		{
		  global $db_sets;
		  global $global_tbl_74;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_74 ORDER BY `id` ");
		  return $get;
		}
		
		function get_admin_reinigung_typ_nach_id($v1)  // tbl - admin_fahrzeug_reinigung
		{
		  global $db_sets;
		  global $global_tbl_74;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_74 WHERE `id`='$v1' LIMIT 1 ");
		  return $get;
		}
		
		
		function get_admin_alle_fahrzeuge_typ()  // tbl - admin_fahrzeug_typ
		{
		  global $db_sets;
		  global $global_tbl_75;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_75 ORDER BY `typ_id` ");
		  return $get;
		}
		
		function get_admin_alle_fahrzeuge_typ_nach_id($v1)  // tbl - admin_fahrzeug_typ
		{
		  global $db_sets;
		  global $global_tbl_75;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_75 WHERE `typ_id`='$v1' LIMIT 1 ");
		  return $get;
		}
		
		
		

		function get_kunden_die_letzten_10()  // tbl - kunde_daten
		{
		  global $db_sets;
		  global $global_tbl_150;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_150 ORDER BY `kid` DESC  LIMIT 10 ");
		  return $get;
		}

		function get_last_kunden_nummer() // tbl_ - kunde_daten
		{
		  global $db_sets;
		  global $global_tbl_150;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_150 ORDER BY `kid` DESC LIMIT 1 ");
		  return $get;
		}	

	  function get_all_kunden_data_fuer_tabelle($v1) // Werte aus Tabelle user_login, user_name  LIKE '$v1%' 
	  {
		global $db_sets;
		global $global_tbl_150;
		$get = $db_sets->query("SELECT * FROM $global_tbl_150 WHERE `name` LIKE '$v1%' ORDER BY `name` ASC ");
		return $get;
	  }	

	  function get_one_kunden_data($v1) //  
	  {
		global $db_sets;
		global $global_tbl_150;
		$get = $db_sets->query("SELECT * FROM $global_tbl_150 WHERE `kid` = '$v1' LIMIT 1 ");
		return $get;
	  }	

	  function get_all_kunden_autos_data_fuer_tabelle($v1)  
	  {
		global $db_sets;
		global $global_tbl_150,$global_tbl_151;
		$get = $db_sets->query("SELECT * FROM $global_tbl_150,$global_tbl_151 WHERE $global_tbl_150.kid  = $global_tbl_151.kid  AND $global_tbl_151.kenn_zeichen LIKE '$v1%' ORDER BY $global_tbl_150.name ASC ");
		return $get;
	  }	


		function get_alle_kunden_autos_nach_kid($v1) // tbl_ - kunde_auto
		{
		  global $db_sets;
		  global $global_tbl_151;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_151 WHERE `kid`='$v1' ORDER BY `id` ");
		  return $get;
		}	

		function get_alle_kunden_autos_nach_id($v1) // tbl_ - kunde_auto
		{
		  global $db_sets;
		  global $global_tbl_151;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_151 WHERE `id`='$v1' LIMIT 1 ");
		  return $get;
		}	

		function get_kunden_wieviel_autos_hat_er($v1)  // tbl - kunde_daten
		{
		  global $db_sets;
		  global $global_tbl_151;
		  $get = $db_sets->query("SELECT  sum($global_tbl_151.aktiv) AS summe FROM $global_tbl_151 WHERE `kid`='$v1' ");
		  return $get;
		}
		
		
		function get_kunde_angebot_nach_kunde_und_auto($v1,$v2,$v3) // tbl_ - kunde_angebot
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_152 WHERE `kunden_id`='$v1' AND `auto_id`='$v2' AND `datum`='$v3' AND `aktiv`='1' ");
		  return $get;
		}	
		
		function get_kunde_angebot_letzter_id() // tbl_ - kunde_angebot
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_152 ORDER BY `id` DESC LIMIT 1 ");
		  return $get;
		}	
		
		function get_kunde_angebot_nach_angebot_id($v1) // tbl_ - kunde_angebot
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_152 WHERE `angebot_id`='$v1' AND `aktiv`='1' ORDER BY `id` ");
		  return $get;
		}	
		
		
		function get_kunde_angebot_summe($v1)  // tbl - kunde_daten
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $get = $db_sets->query("SELECT  sum($global_tbl_152.brutto) AS summe FROM $global_tbl_152 WHERE `kunden_id`='$v1' AND `aktiv`='1' ");
		  return $get;
		}
		
		function get_kunde_angebot_summe_nach_angebot_id($v1)  // tbl - kunde_daten
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $get = $db_sets->query("SELECT  sum($global_tbl_152.brutto) AS summe FROM $global_tbl_152 WHERE `angebot_id`='$v1' AND `aktiv`='1' ");
		  return $get;
		}
		
		function get_kunde_angebot_summe_kunden_und_fahrzeug($v1)  // tbl - kunde_daten
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $get = $db_sets->query("SELECT angebot_id,auto_id,beschreibung,sum($global_tbl_152.brutto) AS summe FROM $global_tbl_152 WHERE `kunden_id`='$v1' AND `aktiv`='1' GROUP BY `auto_id`,`angebot_id` ");
		  return $get;
		}
		
		
		// ================== check FUNCS ============= //
		
		
		
		function check_kunden_daten_control_kid($v1)  // tbl - 150
		{
		  global $db_sets;
		  global $global_tbl_150;
		  $get = $db_sets->query("SELECT last_stamp FROM $global_tbl_150 WHERE `kid`='$v1' LIMIT 1");
		  $num = $db_sets->num_rows($get);
		  return $num;
		}

		function check_ob_angebot_fuer_auto_schon_da($v1,$v2,$v3)  // tbl - kunde_angebot
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $get = $db_sets->query("SELECT id FROM $global_tbl_152 WHERE `kunden_id`='$v1' AND `auto_id`='$v2' AND `datum`='$v3' AND `aktiv`='1' LIMIT 1");
		  $num = $db_sets->num_rows($get);
		  return $num;
		}


		// ================== INSERT FUNCS ============= //
		function insert_into_admin_preispakete_neu($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11) // tbl - admin_preispakete
		{
		  global $db_sets;
		  global $global_tbl_27;
		  $ins = $db_sets->query("INSERT INTO $global_tbl_27(`r_id`,`reinigung`,`k_id`,`kundentyp`,`beschreibung`,`typ_id`,`typ_name`,`typ_beschreibung`,`netto`,`mwst`,`brutto`) VALUES ('$v1','$v2','$v3','$v4','$v5','$v6','$v7','$v8','$v9','$v10','$v11')");
		}	
		
		
		
		function insert_into_admin_fahrzeuge_neu($v1,$v2,$v3,$v4,$v5) // tbl - 70
		{
		  global $db_sets;
		  global $global_tbl_70;
		  $ins = $db_sets->query("INSERT INTO $global_tbl_70(`fahrzeug`,`typ_id`,`typ`,`art_id`,`art`) VALUES ('$v1','$v2','$v3','$v4','$v5')");
		}	
		
		function insert_into_kunden_daten_neu($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12,$v13,$v14,$v15,$v16,$v17,$v18) // tbl - 150
		{
		  global $db_sets;
		  global $global_tbl_150;
		  $ins = $db_sets->query("INSERT INTO $global_tbl_150(`kid`,`kategorie_id`,`kategorie`,`typ_id`,`typ`,`anrede_id`,`anrede`,`ansprache`,`firma`,`vname`,`name`,`strasse`,`plz`,`ort`,`telefon`,`mobil`,`email`,`insert_by`) VALUES ('$v1','$v2','$v3','$v4','$v5','$v6','$v7','$v8','$v9','$v10','$v11','$v12','$v13','$v14','$v15','$v16','$v17','$v18')");
		}	
		
		function insert_into_kunden_auto_neu($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12,$v13,$v14) // tbl - kunde_auto
		{
		  global $db_sets;
		  global $global_tbl_151;
		  $ins = $db_sets->query("INSERT INTO $global_tbl_151(`kid`,`f_id`,`fahrzeug`,`typ_id`,`typ`,`art_id`,`art`,`farbe_id`,`farbe`,`innen_id`,`innen`,`kenn_stadt`,`kenn_zeichen`,`insert_by`) VALUES ('$v1','$v2','$v3','$v4','$v5','$v6','$v7','$v8','$v9','$v10','$v11','$v12','$v13','$v14')");
		}	
		
		function insert_into_kunde_angebot_neu($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12,$v13,$v14,$v15,$v16) // tbl - 152
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $ins = $db_sets->query("INSERT INTO $global_tbl_152(`sess_id`,`angebot_id`,`datum`,`kunden_id`,`auto_id`,`r_id`,`reinigung`,`k_id`,`kundentyp`,`beschreibung`,`typ_id`,`typ_name`,`typ_beschreibung`,`netto`,`mwst`,`brutto`) VALUES ('$v1','$v2','$v3','$v4','$v5','$v6','$v7','$v8','$v9','$v10','$v11','$v12','$v13','$v14','$v15','$v16')");
		}	


		// ================== UPDATE FUNCS ============= //
		
		function  update_admin_preispakete($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12) // tbl - admin_preispakete 
		{
		  global $db_sets;
		  global $global_tbl_27;
		  $up1 = $db_sets->query("UPDATE $global_tbl_27 SET `r_id`='$v2' WHERE `id`='$v1'   LIMIT 1");
		  $up2 = $db_sets->query("UPDATE $global_tbl_27 SET `reinigung`='$v3' WHERE `id`='$v1'   LIMIT 1");
		  $up3 = $db_sets->query("UPDATE $global_tbl_27 SET `k_id`='$v4' WHERE `id`='$v1'   LIMIT 1");
		  $up4 = $db_sets->query("UPDATE $global_tbl_27 SET `kundentyp`='$v5' WHERE `id`='$v1'   LIMIT 1");
		  $up5 = $db_sets->query("UPDATE $global_tbl_27 SET `beschreibung`='$v6' WHERE `id`='$v1'   LIMIT 1");
		  $up6 = $db_sets->query("UPDATE $global_tbl_27 SET `typ_id`='$v7' WHERE `id`='$v1'   LIMIT 1");
		  $up7 = $db_sets->query("UPDATE $global_tbl_27 SET `typ_name`='$v8' WHERE `id`='$v1'   LIMIT 1");
		  $up8 = $db_sets->query("UPDATE $global_tbl_27 SET `typ_beschreibung`='$v9' WHERE `id`='$v1'   LIMIT 1");
		  $up9 = $db_sets->query("UPDATE $global_tbl_27 SET `netto`='$v10' WHERE `id`='$v1'   LIMIT 1");
		  $up10 = $db_sets->query("UPDATE $global_tbl_27 SET `mwst`='$v11' WHERE `id`='$v1'   LIMIT 1");
		  $up11 = $db_sets->query("UPDATE $global_tbl_27 SET `brutto`='$v12' WHERE `id`='$v1'   LIMIT 1");
		}	
		
		function  update_admin_fahrzeuge($v1,$v2,$v3,$v4,$v5,$v6) // tbl - admin_fahrzeuge 
		{
		  global $db_sets;
		  global $global_tbl_70;
		  $up1 = $db_sets->query("UPDATE $global_tbl_70 SET `fahrzeug`='$v2' WHERE `id`='$v1'   LIMIT 1");
		  $up2 = $db_sets->query("UPDATE $global_tbl_70 SET `typ_id`='$v3' WHERE `id`='$v1'   LIMIT 1");
		  $up3 = $db_sets->query("UPDATE $global_tbl_70 SET `typ`='$v4' WHERE `id`='$v1'   LIMIT 1");
		  $up4 = $db_sets->query("UPDATE $global_tbl_70 SET `art_id`='$v5' WHERE `id`='$v1'   LIMIT 1");
		  $up5 = $db_sets->query("UPDATE $global_tbl_70 SET `art`='$v6' WHERE `id`='$v1'   LIMIT 1");
		}	
		
		function  update_kunden_auto($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12,$v13,$v14,$v15) // tbl - kunde_auto 
		{
		  global $db_sets;
		  global $global_tbl_151;
		  $up1 = $db_sets->query("UPDATE $global_tbl_151 SET `kid`='$v2' WHERE `id`='$v1'   LIMIT 1");
		  $up2 = $db_sets->query("UPDATE $global_tbl_151 SET `f_id`='$v3' WHERE `id`='$v1'   LIMIT 1");
		  $up3 = $db_sets->query("UPDATE $global_tbl_151 SET `fahrzeug`='$v4' WHERE `id`='$v1'   LIMIT 1");
		  $up4 = $db_sets->query("UPDATE $global_tbl_151 SET `typ_id`='$v5' WHERE `id`='$v1'   LIMIT 1");
		  $up5 = $db_sets->query("UPDATE $global_tbl_151 SET `typ`='$v6' WHERE `id`='$v1'   LIMIT 1");
		  $up6 = $db_sets->query("UPDATE $global_tbl_151 SET `art_id`='$v7' WHERE `id`='$v1'   LIMIT 1");
		  $up7 = $db_sets->query("UPDATE $global_tbl_151 SET `art`='$v8' WHERE `id`='$v1'   LIMIT 1");
		  $up8 = $db_sets->query("UPDATE $global_tbl_151 SET `farbe_id`='$v9' WHERE `id`='$v1'   LIMIT 1");
		  $up9 = $db_sets->query("UPDATE $global_tbl_151 SET `farbe`='$v10' WHERE `id`='$v1'   LIMIT 1");
		  $up10 = $db_sets->query("UPDATE $global_tbl_151 SET `innen_id`='$v11' WHERE `id`='$v1'   LIMIT 1");
		  $up11 = $db_sets->query("UPDATE $global_tbl_151 SET `innen`='$v12' WHERE `id`='$v1'   LIMIT 1");
		  $up12 = $db_sets->query("UPDATE $global_tbl_151 SET `kenn_stadt`='$v13' WHERE `id`='$v1'   LIMIT 1");
		  $up13 = $db_sets->query("UPDATE $global_tbl_151 SET `kenn_zeichen`='$v14' WHERE `id`='$v1'   LIMIT 1");
		  $up14 = $db_sets->query("UPDATE $global_tbl_151 SET `insert_by`='$v15' WHERE `id`='$v1'   LIMIT 1");
		}	
		
		
		function  update_kunde_angebot_zustand($v1,$v2) // tbl - user_login 
		{
		  global $db_sets;
		  global $global_tbl_152;
		  $up1 = $db_sets->query("UPDATE $global_tbl_152 SET `aktiv`='$v2' WHERE `id`='$v1' LIMIT 1");
		}	
		
		function  update_kunuser_login_session_0asf1($v1,$v2,$v3) // tbl - user_login 
		{
		  global $db_sets;
		  global $global_tbl_208;
		  $up1 = $db_sets->query("UPDATE $global_tbl_208 SET `session_id`='$v2' WHERE `uname`='$v1'   LIMIT 1");
		  $up2 = $db_sets->query("UPDATE $global_tbl_208 SET `md5`='$v3' WHERE `uname`='$v1'   LIMIT 1");
		}	
		
		
?>