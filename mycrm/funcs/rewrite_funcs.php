<?php


/*
*
* NONE MYSQL FUNCTIONS
*
*/

  function format_euro($value) // tausenderstellen getrennt und keine Nachkommastellen
  {
   return number_format($value, 0, ',', '.');
  }	 
 
  function format_euro_2($value) // tausenderstellen getrennt und 2 Nachkommastellen
  {
	 return number_format($value, 2, ',', '.');
  }	 

function rerwite_url_1($v1,$v2)
{
	$first   = strtolower($v1);
	$second  = intval($v2);
	
	if (strlen($second) == "1") { $second = "000".$second; }
	if (strlen($second) == "2") { $second = "000".$second; }
	if (strlen($second) == "3") { $second = "00".$second; }
	if (strlen($second) == "4") { $second = "0".$second; }
	
  $first   = str_replace(" ", "-", $first);
  $first   = str_replace("&", "und", $first);
  $first   = str_replace("--", "-", $first);
  $first   = str_replace("�", "ae", $first);
  $first   = str_replace("�", "oe", $first);
  $first   = str_replace("�", "ue", $first);
  $first   = str_replace("�", "ss", $first);
	$string_ = $first.",".$second.".html";
	return $string_;
}	

function rerwite_url_2($v1,$v2)
{
	$first   = strtolower($v1);
	$second  = intval($v2);
	
	if (strlen($second) == "1") { $second = "000".$second; }
	if (strlen($second) == "2") { $second = "000".$second; }
	if (strlen($second) == "3") { $second = "00".$second; }
	if (strlen($second) == "4") { $second = "0".$second; }
	
  $first   = str_replace(" ", "-", $first);
  $first   = str_replace("&", "und", $first);
  $first   = str_replace("--", "-", $first);
  $first   = str_replace("�", "ae", $first);
  $first   = str_replace("�", "oe", $first);
  $first   = str_replace("�", "ue", $first);
  $first   = str_replace("�", "ss", $first);
	$string_ = "kommentare_".$first.",".$second.".html";
	return $string_;
}	


function rerwite_url_3($v1)
{
	  $first   = $v1;
	  $first   = str_replace(" ", "-", $first);
	  $first   = str_replace("&", "und", $first);
	  $first   = str_replace("--", "-", $first);
	  $first   = str_replace("ü", "ue", $first);
	  $first   = str_replace("ä", "ae", $first);
	  $first   = str_replace("ö", "oe", $first);
	  $first   = str_replace("ß", "ss", $first);
  	  $first   = strtolower($first);
  return $first;

}	

function rerwite_url_10($v1,$v2)
{

  $first   = trim(utf8_decode($v1));
	$first   = strtolower($first);
	$second  = intval($v2);
	
	if (strlen($second) == "1") { $second = "000".$second; }
	if (strlen($second) == "2") { $second = "000".$second; }
	if (strlen($second) == "3") { $second = "00".$second; }
	if (strlen($second) == "4") { $second = "0".$second; }
	
  $first   = str_replace(" ", "-", $first);
  $first   = str_replace("&", "und", $first);
  $first   = str_replace("--", "-", $first);
  $first   = str_replace("�", "ae", $first);
  $first   = str_replace("�", "oe", $first);
  $first   = str_replace("�", "ue", $first);
  $first   = str_replace("�", "ss", $first);
  $first   = str_replace("?", "", $first);
  $first   = str_replace("!", "", $first);
  $first   = str_replace(":", "", $first);
  $first   = str_replace(".", "-", $first);
	$string_ = "news-".$first.",".$second.".html";
	// echo "<strong>$string_</strong><br />";
	return $string_;
}	



function set_utf8($first)
{
  $first   = str_replace("ü", "�", $first);
  $first   = str_replace("Ü", "�", $first);
  $first   = str_replace("ä", "�", $first);
  $first   = str_replace("Ä", "�", $first);
  $first   = str_replace("ö", "�", $first);
  $first   = str_replace("Ö�", "�", $first);
  $first   = str_replace("ß", "�", $first);
  return $first;
}	




function set_google_utf8($first)
{
  $first   = str_replace("�", "ue", $first);
  $first   = str_replace("�", "Ue", $first);
  $first   = str_replace("�", "ae", $first);
  $first   = str_replace("�", "Ae", $first);
  $first   = str_replace("�", "oe", $first);
  $first   = str_replace("�", "Oe", $first);
  $first   = str_replace("�", "ss", $first);
  return $first;
}	

function set_double($first)
{
  $first   = str_replace(",", ".", $first);
  return $first;
}	





?>