<?php
		// ================== GET FUNCS ============= //
		
		function get_admin_alle_user_anfragen_aktiv()  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_15;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_15 WHERE `aktiv`='1' AND `freigegeben`='0' ORDER BY `id` DESC ");
		  return $get;
		}
		
		
	  function get_admin_alle_user_anfragen_aktiv_01($v1) // Werte aus Tabelle user_login, user_name  LIKE '$v1%' 
	  {
		global $db_sets;
		global $global_tbl_15;
		$get = $db_sets->query("SELECT * FROM $global_tbl_15 WHERE `user_name` LIKE '$v1%' ORDER BY `user_name` ASC ");
		return $get;
	  }	
	  
	  function get_admin_alle_user_anfragen_aktiv_02($v1,$v2) // Werte aus Tabelle user_login, user_name  LIKE '$v1%' 
	  {
		global $db_sets;
		global $global_tbl_15;
		$get = $db_sets->query("SELECT * FROM $global_tbl_15 WHERE `user_name` LIKE '$v1%'  AND `von`='$v2'  ORDER BY `user_name` ASC ");
		return $get;
	  }	
	  
	  
		function get_admin_alle_user_anfragen_aktiv_03($v1)  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_15;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_15 WHERE `aktiv`='1' AND `freigegeben`='0'  AND `von`='$v1' ORDER BY `id` DESC ");
		  return $get;
		}
	  
	  
	  
		function get_admin_alle_user_anfragen_aktiv_von($v1)  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_15;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_15 WHERE `aktiv`='1' AND `freigegeben`='0' AND `von`='$v1' ORDER BY `id` DESC ");
		  return $get;
		}
		
		function get_admin_alle_user_anfragen_aktiv_nav()  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_15;
		  $get = $db_sets->query("SELECT  COUNT(`id`) AS `summe`  FROM $global_tbl_15 WHERE `aktiv`='1' AND `freigegeben`='0' ORDER BY `id` DESC ");
		  return $get;
		}
		
		
		function get_admin_alle_user_anfragen_aktiv_von_nav($v1,$v2)  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_15;
		  $get = $db_sets->query("SELECT COUNT(`id`) AS `summe` FROM $global_tbl_15 WHERE `aktiv`='1' AND `freigegeben`='0' AND `von`='$v1' ORDER BY `id` DESC ");
		  return $get;
		}
		
		
		
		
		function get_admin_user_nach_id($v1)  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_15;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_15 WHERE `id`='$v1' LIMIT 1 ");
		  return $get;
		}
		

		function get_admin_video_alle_gruppen($v1)  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_20;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_20 ORDER BY `id` ASC ");
		  return $get;
		}

		function get_admin_video_alle_gruppen_von_bis($v1,$v2)  // tbl - user_video
		{
		  global $db_sets;
		  global $global_tbl_20;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_20 WHERE `id`>='$v1' AND `id`<='$v2' ORDER BY `id` ASC ");
		  return $get;
		}



		function get_admin_video_alle_landingpage_nach_id($v1)  // tbl - user_video_php
		{
		  global $db_sets;
		  global $global_tbl_21;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_21 ORDER BY `id` ASC ");
		  return $get;
		}
		
		function get_admin_video_alle_landingpage_eine_id($v1)  // tbl - user_video_php
		{
		  global $db_sets;
		  global $global_tbl_21;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_21 WHERE `id`='$v1' ");
		  return $get;
		}

		function get_admin_tage_wort_alle()  // tbl - user_tage_wort
		{
		  global $db_sets;
		  global $global_tbl_22;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_22 ORDER BY `id` ASC ");
		  return $get;
		}

		function get_admin_zahlunggsart_nach_id($v1)  // tbl - user_tage_wort
		{
		  global $db_sets;
		  global $global_tbl_28;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_28 WHERE `id`='$v1' LIMIT 1 ");
		  return $get;
		}

		function get_admin_zahlung_nach_id($v1)  // tbl - user_tage_wort
		{
		  global $db_sets;
		  global $global_tbl_29;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_29 WHERE `id`='$v1' LIMIT 1 ");
		  return $get;
		}

		function get_admin_tage_alle()  // tbl - user_tage
		{
		  global $db_sets;
		  global $global_tbl_14;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_14 ORDER BY `id` ASC ");
		  return $get;
		}

		function get_admin_monate_alle()  // tbl - user_tage
		{
		  global $db_sets;
		  global $global_tbl_13;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_13 ORDER BY `id` ASC ");
		  return $get;
		}

		function get_admin_jahre_ab_heue_plus($v1)  // tbl - user_tage
		{
		  global $db_sets;
		  global $global_tbl_25;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_25 WHERE  `jahre`<='$v1' ORDER BY `id` ASC ");
		  return $get;
		}

		function get_admin_stunde_alle()  // tbl - user_stunden
		{
		  global $db_sets;
		  global $global_tbl_23;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_23 ORDER BY `id` ASC ");
		  return $get;
		}

		function get_admin_minuten_alle()  // tbl - user_stunden
		{
		  global $db_sets;
		  global $global_tbl_24;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_24 ORDER BY `id` ASC ");
		  return $get;
		}

		function get_admin_dauerzeit_alle()  // tbl - user_stunden
		{
		  global $db_sets;
		  global $global_tbl_26;
		  $get = $db_sets->query("SELECT * FROM $global_tbl_26 ORDER BY `id` ASC ");
		  return $get;
		}

		// ================== CHECK FUNCS ============= //
		
		
		function check_online_page($v1,$v2)  // tbl - online_page
		{
		  global $db_sets;
		  global $global_tbl_101;
		  $get = $db_sets->query("SELECT  `aktiv` FROM $global_tbl_101 WHERE  `session_id`='$v1' AND `webinar_id`='$v2' LIMIT 1");
		  $num = $db_sets->num_rows($get);
		  return $num;
		}


		// ================== INSERT FUNCS ============= //
		
		function insert_neu_online_page($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11) // tbl - online_page
		{
		  global $db_sets;
		  global $global_tbl_101;
		  $ins = $db_sets->query("INSERT INTO $global_tbl_101(`session_id`,`ip`,`uid`,`uid_vermittler`,`seite`,`seite_id`,`webinar_id`,`tag`,`monat`,`jahr`,`datum`) VALUES ('$v1','$v2','$v3','$v4','$v5','$v6','$v7','$v8','$v9','$v10','$v11')");
		}	
		
		//================= UPDATE FUNCS ==============//
		
		function  update_admin_user_anfragen_sperren($v1,$v2) // tbl - admin_user_anfragen 
		{
		  global $db_sets;
		  global $global_tbl_15;
		  $up1 = $db_sets->query("UPDATE $global_tbl_15 SET `aktiv`='$v2' WHERE `id`='$v1' LIMIT 1");
		}	
		

?>