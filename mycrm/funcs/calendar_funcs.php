<?


/*
*
* Return human (german) readable Time
*
*/

function set_german_date($value) 
{
   $year  = substr($value, 0, 4);
   $month = substr($value, 5, 2);
   $day   = substr($value, 8, 2);
   $my_date = $day .".". $month .".". $year;
   return $my_date;
}

function compare_two_mysql_dates($date1,$date2) 
{
  $date_1  = substr($date1,0,10);
  $date_2  = substr($date2,0,10);
  return strcmp($date_1,$date_2);
}

function set_german_datetime_without_year($value) 
{
   $month = substr($value, 5, 2);
   $day   = substr($value, 8, 2);
   $time  = substr($value, 10, 6);
   $my_datetime = $day .".". $month ." ". $time;
   return $my_datetime;
}	

function set_german_datetime($value) 
{
   $year  = substr($value, 0, 4);
   $month = substr($value, 5, 2);
   $day   = substr($value, 8, 2);
   $time  = substr($value, 10, 6);

   $my_datetime = $day .".". $month .".". $year . " ". $time;
   return $my_datetime;
}

function set_german_time($value) {

   // $year  = substr($value, 0, 4);
   // $month = substr($value, 5, 2);
   // $day   = substr($value, 8, 2);
   $time  = substr($value, 10,6);

   $my_datetime = $time;
   return $my_datetime;
}

function set_mysql_date($value) {

  $year  = substr($value,6,4);
  $month = substr($value,3,2);
  $day   = substr($value,0,2);
  $time  = substr($value,10,strlen($value));

  $my_mysql_date = $year ."-". $month ."-". $day ." ". $time;
  return $my_mysql_date;
}




?>