<?php

if (!isset($_SESSION['immobilienberechner']))
{
 session_name("immobilienberechner");
 session_start();
 if (strlen(session_id()) != 32) 
 {
  mt_srand ((double)microtime()*1000000);
  session_id(md5(uniqid(mt_rand()))); 
 }
 setcookie(session_name(), session_id());
}
else
{
 include "mycrm/lib/class.session.php"; //session-klasse
 $session = new Session($my_session);
} 

$sess_id = session_id();
$ip      = $_SERVER['REMOTE_ADDR']; 

include "mycrm/lib/class.register.php"; // register-klasse
$register = new Register();
$register->check_id($sess_id,$ip,_BASE_);

include "mycrm/lib/class.auth.php"; // authentfizierungs-klasse
$auth = new Auth();
$auth->check_user($sess_id);


include "mycrm/lib/class.secure.php"; // security-klasse
$sec = new Secure();

if (_BASE_ == "check_login.php")
{
 include "mycrm/lib/class.login.php"; // authentfizierungs-klasse
 $chk = new Login("1");
} 

?>