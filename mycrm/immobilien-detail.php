<?php
define("_BASE_","immobilien-detail.php");  // filename (basename perl&php);
$stamp = filemtime(_BASE_);
$last_touch =  date("d.m.Y", $stamp);
$stunde  = date("H");
function utime ()
{
$time = explode( " ", microtime());
$usec = (double)$time[0];
$sec = (double)$time[1];
return $sec + $usec;
}
$start = utime();
$stamp = time();
$heute = gmdate("d m Y H:i:s" , $stamp);
$tag  = date("d");
$monat  = date("m");
$jahr  = date("Y");
$uhr  = date("G");
$minute  = date("i");
$datum = $tag.".".$monat.".".$jahr;
$datum_01 = $jahr."-".$monat."-".$tag;


require("global_funcs.php");
require("inc/lib_incl_intern.php");
?>

<?php

/*
if (!defined('_IS_VALID_')  || $auth->prio <= "0")
{
  echo "Unerlaubter Zugriff.....";
  echo "</body>";
  echo "</html>";
  exit();
}

else
{*/
include "inc/head.inc.php";
include "inc/header.php";

// Hier wird die Nav nach der Prio  aus gegeben ! 
include "nav/nav.php";

//}


 

?>


<!-- ==============CONTENT============== -->
  
      <main>
      
      <section class="well11">
      	<div class="container-fluid">
          <div class="grid_11"></div>
          <div class="grid_1">
            <h4 class="fa">
                <a href="immobilien.php" title="Zurück">
                    <i class="fa fa-times-circle fa-lg" aria-hidden="true" title="zurück"></i>
                </a>
            </h4>
          </div>
      	</div>
      </section>


<?php

		echo "<section class=\"well1\">";
			echo "<div class=\"container-fluid\">";
			
				//Variablen
				$vtext01 = 'denkmal'; 
				$vtext02 = 'images/test/immobilie_1.jpg'; //Pfad Bild von Immobilie
				$vtext03 = 'Denkmalobjekt'; //Objektart
				$vtext04 = 'Sachsen'; //Bundesland
				$vtext05 = number_format('126000', 2, ',', '.') ." €";//Kaufpreis
				$vtext06 = '1950'; //Baujahr
				$vtext07 = 'Ludwig von Annaberg Str. 17'; //Straße
				$vtext08 = 'Leipzig'; //Ort
				$vtext09 = 14; //frei
				$vtext10 = 4; //reserviert
				$vtext11 = 5; //verkauft
				$vtext12 = 1; //ID
				$vtext13 = 1; //ID ob Bauträger oder Mensch (Bsp. 1 = Bauträger)
				$vtext14 = 'Das Baudenkmal GmbH'; //Name Bauträger oder Mensch
				$vtext15 = 0; //ID für Immobiliendetails anschauen 
				
				echo "<div class='immobilie detail'>";
					echo "<div class='grid_4 bild-container'>";
						echo "<div class='$vtext01' style='background-image: url($vtext02)'>";
							echo "<img src='$vtext02' />";
						echo "</div>";
					echo "</div>";
					
					echo "<div class='grid_8 info-container'>";
					
						echo "<div class='grid_4 info'>";
							echo "<img class='icon_haus' src='images/haus.svg' />";
							echo "<div>";
								echo "<p>$vtext03</p>";
								echo "<p>$vtext04</p>";
							echo "</div>";
						echo "</div>";	
						
						echo "<div class='grid_4 info'>";
							echo "<div>";
								echo "<p>Kaufpreis: <span>ab $vtext05</span></p>";
								echo "<p>Baujahr: <span>$vtext06</span></p>";
							echo "</div>";
						echo "</div>";
							
						echo "<div class='grid_4 info ".(($vtext13 == 1) ? "bautraeger" : "")."'>";
							echo "<div></div>";
							echo "<p>$vtext14</p>";	
						echo "</div>";
						
						echo "<div class='grid_4 info'>";
							echo "<p>$vtext07</p>";
							echo "<p><img class='icon_ort' src='images/standort.svg' /> <strong>$vtext08</strong></p>";
						echo "</div>";
							
						echo "<div class='grid_4 info'>";
							echo "<div>";
								echo "<p>Einheiten:</p>";
							echo "</div>";
							echo "<div>";
								echo "<p>".(($vtext09 < 10) ? "&nbsp; " : "")."$vtext09 &nbsp;&nbsp;<span class='kreis_gruen'></span>&nbsp;&nbsp;FREI&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>";
								echo "<p>".(($vtext10 < 10) ? "&nbsp; " : "")."$vtext10 &nbsp;&nbsp;<span class='kreis_gelb'></span>&nbsp;&nbsp;RESERVIERT</p>";
								echo "<p>".(($vtext11 < 10) ? "&nbsp; " : "")."$vtext11 &nbsp;&nbsp;<span class='kreis_rot'></span>&nbsp;&nbsp;VERKAUFT&nbsp; &nbsp;</p>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4 info back'>";
							echo "<div>";
								echo "<a class='btn14' href='immobilien.php'><img src='images/zurueck.png' /></a>";
							echo "</div>";
						echo "</div>";
							
					echo "</div>"; //ENDE grid_8
				echo "</div>";
			echo "</div>";
		echo "</section>";
		
		echo "<section>";
			echo "<div class=\"container-fluid\">";
				echo "<div class='grid_11'></div>";
				echo "<div class='grid_1'>";
					echo "<h3 class='gruen'>";
						echo "<a href='einheit-anlegen.php'>";
							echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true' title='Neue Immobilie anlegen'></i>";
						echo "</a>";
					echo "</h3>";
				echo "</div>";
			echo "</div>";
		echo "</section>";
		
		echo "<section class=\"well1\">";
			echo "<div class=\"container-fluid\">";
				echo "<div class=\"grid_12\">";
					echo "<table class='js-resp-table detail-tbl'>";
 						echo "<thead>";
                        	echo "<tr> ";                            
								echo "<th>NR.</th>";
								echo "<th>BEZ. / LAGE</th>";
								echo "<th data-breakpoints='xs'>GRUNDRISS</th>";
								echo "<th data-breakpoints='xs'>ZIMMER</th>";
								echo "<th data-breakpoints='xs sm'>QM</th>";
								echo "<th data-breakpoints='xs sm md'>MIETE</th>";
								echo "<th data-breakpoints='xs sm'>KAUFPREIS</th>";
								echo "<th data-breakpoints='xs sm md'>ZUSATZKOSTEN</th>";
								echo "<th data-breakpoints=''>BERECHNUNG</th>";
                         	echo "</tr>";
						echo "</thead>";
						
						echo "<tbody>";
						//PHP Schleife starten
							
							$var01 = 1; //Nummerierung muss in der Schleife hochgezählt werden
							$var02 = 'H1 - WE 1'; //Bezeichnung
							$var03 = 'EG - RE'; //Lage
							$var04 = 'images/test/grundriss.png'; //Grundriss
							$var05 = 3; //Anzahl Zimmer
							$var06 = number_format(84.20, 2, ',', '.'); //qm
							$var07 = number_format(12.10, 2, ',', '.')." €"; //Mietpreis qm
							$var08 = number_format(1018.82, 2, ',', '.')." €"; //Mietpreis ges.
							$var09 = number_format(352600, 2, ',', '.')." €"; //Kaufpreis
							$var10 = number_format(5000, 2, ',', '.')." €"; //Stellplatz
							$var11 = number_format(15000, 2, ',', '.')." €"; //TG
							$var12 = number_format(3600, 2, ',', '.')." €"; //Küche
							$var13 = 21; //Anzahl der Berechnungen
							$var14 = 'frei'; //Wohnungsbelegung
							
							echo "<tr>";
								echo "<td><p>$var01</p></td>";
								echo "<td><p>$var02</p><p>$var03</p></td>";
								echo "<td><div class='lupe js-lupe'><img src='$var04' /></div></td>";
								echo "<td><p>$var05</p></td>";
								echo "<td><p>$var06</p></td>";
								echo "<td><p>$var07</p><p>$var08</p></td>";
								echo "<td><p>$var09</p></td>";
								echo "<td class='kosten'>";
									echo "<p>Stellplatz <span>$var10</span></p>";
									echo "<p>TG <span>$var11</span></p>";
									echo "<p>Küche <span>$var12</span></p>";
								echo "</td>";
								echo "<td><img src='images/rechnen_$var14.svg' /><p>$var13</p></td>";
							echo "</tr>";
							
						//PHP Schleife beenden
						echo "</tbody>";
					echo "</table>";
				echo "</div>";
			echo "</div>";
		echo "</section>";

          ?>
          
		<section id="overlay">
        	<div class="middle-container">
            	<div>
            		<img src='images/test/grundriss.png' />
                </div>
            </div>
        </section>

      </main>
      
<script> 
	$( document ).ready(function() { 		
		$('.js-resp-table').footable();
		
		$('.js-lupe').click(function(){
			$('#overlay').show();
		});	
		
		$('#overlay').click(function(){
			$('#overlay').hide();
		});	
	});  
</script>   
     
      
<!-- ==============FOOTER============== -->
                      
<?php      
 include ("inc/end.php");
?>