<?php
define("_BASE_","immobilien-anlegen.php");  // filename (basename perl&php);
$stamp = filemtime(_BASE_);
$last_touch =  date("d.m.Y", $stamp);
$stunde  = date("H");
function utime ()
{
$time = explode( " ", microtime());
$usec = (double)$time[0];
$sec = (double)$time[1];
return $sec + $usec;
}
$start = utime();
$stamp = time();
$heute = gmdate("d m Y H:i:s" , $stamp);
$tag  = date("d");
$monat  = date("m");
$jahr  = date("Y");
$uhr  = date("G");
$minute  = date("i");
$datum = $tag.".".$monat.".".$jahr;
$datum_01 = $jahr."-".$monat."-".$tag;


require("global_funcs.php");
require("inc/lib_incl_intern.php");
?>

<?php

/*
if (!defined('_IS_VALID_')  || $auth->prio <= "0")
{
  echo "Unerlaubter Zugriff.....";
  echo "</body>";
  echo "</html>";
  exit();
}

else
{*/
include "inc/head.inc.php";
include "inc/header.php";

// Hier wird die Nav nach der Prio  aus gegeben ! 
include "nav/nav.php";

//}


  

?>


<!-- ==============CONTENT============== -->
  
      <main>


<?php

		echo "<form class='immobilie-anlegen'>";
			
			echo "<section class=\"well1\">";
				echo "<div class=\"container-fluid\">";
					//Objektbild
						echo "<div class='grid_6'>";
							echo "<div class='header-blau'><p>Objektbild </p></div>";
							echo "<div id='bild-hochladen'>";
							?>
								<div class="slim ins7"
									 data-min-size="500,350"
									 data-size="500,350"
									 data-force-size="500,350"
									 data-post="output"
									 data-status-image-too-small="Ihr Bild muss mindestens eine Größe von 500 x 350px haben!"
									 data-label="<p id='upload-text'><br>Hier klicken oder per Drag and Drop <br>das Bild hier einfügen.</p>"
									 data-button-cancel-label="Abbrechen"
									 data-button-confirm-label="Übernehmen"
									 data-max-file-size="2"
									 data-save-initial-image="true">
									<input type="file" name="bild-upload"/>
								</div>
                            <?php
							echo "</div>";
						echo "</div>";
					/**/
					//Objektgrunddaten
					echo "<div class='grid_6'>";
						echo "<div class='header-blau'><p>Objektgrunddaten <span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						
						echo "<label for='objektart'>Objektart</label>";
						echo "<select id='objektart'>";
							echo "<option value=''>Objektart</option>";
							echo "<option value='1'>Neubau</option>";
							echo "<option value='2'>Altbau</option>";
							echo "<option value='3'>Denkmal</option>";
						echo "</select>";
						
						echo "<label for='verkaufer'>Verkäufer/Bauträger</label>";
						echo "<input type='text' name='verkaufer' placeholder='Name Verkäufer oder Bauträger'/>";
						
						echo "<div class='input-strasse-hnr'>";
							echo "<label for='strasse'>Straße</label>";
							echo "<label for='hnr'>Hausnummer</label>";
							echo "<input type='text' name='strasse' placeholder='Straße'/>";
							echo "<input type='text' name='hnr' placeholder='HausNr'/>";
						echo "</div>";
						
						echo "<div class='input-plz-ort'>";
							echo "<label for='plz'>PLZ</label>";
							echo "<label for='ort'>Ort</label>";
							echo "<input type='text' name='plz' placeholder='PLZ'/>";
							echo "<input type='text' name='ort' placeholder='Ort'/>";
						echo "</div>";
						
						echo "<label for='bundesland'>Objektart</label>";
						echo "<select id='bundesland'>";
							echo "<option value=''>Bundesland</option>";
							//Aus DB holen
							echo "<option value='1'>Bayern</option>";
							echo "<option value='2'>Baden-Württemberg</option>";
							/**/
						echo "</select>";
					echo "</div>";
					/**/
					//zusätzliche Grunddaten
					echo "<div class='grid_12 well2 grunddaten'>";
						echo "<div class='header-blau'><p>Zusätzliche Grunddaten <span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='baujahr'>Baujahr</label>";
							echo "<div>";
								echo "<select name='baujahr'>";
									echo "<option value=''>Baujahr wählen</option>";
									for($i = $jahr; $i >= 1800 ; $i--){
										echo "<option value='$i'>$i</option>";
									}
								echo "</select>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='fertigstellung'>Fertigstellung</label>";
							echo "<div>";
								echo "<input type='text' class='datepicker' name='fertigstellung' placeholder='Datum Fertigstellung'>";
								echo "<i class='fa-calendar js-calendar'></i>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<label for='nutzungsbeginn'>Nutzungsbeginn</label>";
							echo "<div>";
								echo "<input type='text' class='datepicker' name='nutzungsbeginn' placeholder='Datum Nutzungsbeginn'>";
								echo "<i class='fa-calendar js-calendar'></i>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
					/**/	
					//Grunddaten Kaufpreisaufteilung
					echo "<div class='grid_12 grunddaten_kaufpries'>";
						echo "<div class='header-blau'><p>Grunddaten zur Kaufpreisaufteilung <span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_6'>";
							echo "<label for='grund'>Grund & Boden</label>";
							echo "<div>";
								echo "<input type='text' name='grund' placeholder='Grund & Boden'/>";
							echo "</div>";
							echo "<div>";
								echo "<input type='text' name='grund_proz' placeholder='0,00'/>";
							echo "</div>";
							
							echo "<label for='altbausubstanz'>Altbausubstanz</label>";
							echo "<div>";
								echo "<input type='text' name='altbausubstanz' placeholder='Altbausubstanz'/>";
							echo "</div>";
							echo "<div>";
								echo "<input type='text' name='altbausubstanz_proz' placeholder='0,00'/>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_6'>";
							echo "<label for='neubau'>Herstellungskosten Neubau</label>";
							echo "<div>";
								echo "<input type='text' name='neubau' placeholder='Herstellungskosten'/>";
							echo "</div>";
							echo "<div>";
								echo "<input type='text' name='neubau_proz' placeholder='0,00'/>";
							echo "</div>";
							
							echo "<label for='sanierungskosten'>Sanierungskosten</label>";
							echo "<div>";
								echo "<input type='text' name='sanierungskosten' placeholder='Sanierungskosten'/>";
							echo "</div>";
							echo "<div>";
								echo "<input type='text' name='sanierungskosten_proz' placeholder='0,00'/>";
							echo "</div>";
						echo "</div>";
						
					echo "</div>";
					/**/	
					
					//Grunddaten Erwerbsnebenkosten
					echo "<div class='grid_12 well2 grid_row erwerbsnebenkosten'>";
						echo "<div class='header-blau'><p>Grunddaten zu den Erwerbsnebenkosten<span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_5'>";
							echo "<label for='nebenkosten_bezeichnung'>Bezeichnung</label>";
							echo "<div>";
								echo "<input type='text' name='nebenkosten_bezeichnung' placeholder='Bezeichnung'/>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_3'>";
							echo "<label for='nebenkosten_typ'>Typ</label>";
							echo "<div>";
								echo "<select name='nebenkosten_typ'>";
									echo "<option value=''>Typ wählen</option>";
									echo "<option value='werbungskosten'>Werbungskosten</option>";
								echo "</select>";	
							echo "</div>";	
						echo "</div>";
						
						echo "<div class='grid_2'>";
							echo "<label for='nebenkosten_proz'>Prozent</label>";
							echo "<div class='proz'>";
								echo "<input type='text' name='nebenkosten_proz' placeholder='0,00'/>";
							echo "</div>";		
						echo "</div>";
						
						echo "<div class='grid_2'>";
							echo "<label for='nebenkosten_jahr'>Jahr</label>";
							echo "<div>";
								echo "<input type='text' name='nebenkosten_jahr' placeholder='Jahr'/>";
							echo "</div>";	
						echo "</div>";
						
						echo "<input type='hidden' id='id' value='1' />";
						echo "<div id='addFields'></div>";
						
						echo "<div class='grid_11'>";
						echo "</div>";
						echo "<div class='grid_1'>";
							echo "<h3 class='gruen'>";
								echo "<a href='' class='js-add-form-fields'>";
									echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true'></i>";
								echo "</a>";
							echo "</h3>";
						echo "</div>";
						
						
					echo "</div>";
					/**/	
					
					//Grunddaten laufende Nebenkosten
					echo "<div class='grid_12 grid_row lauf_nebenkosten'>";
						echo "<div class='header-blau'><p>Grunddaten zu den laufenden monatlichen Nebenkosten<span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						echo "<div class='grid_12 grid_row'>";
							echo "<div></div>";
							echo "<div class='grid_5'>";
								echo "<label for='lauf_nebenkosten_bezeichnung'>Bezeichnung</label>";
								echo "<div>";
									echo "<input type='text' name='lauf_nebenkosten_bezeichnung' placeholder='Bezeichnung'/>";
								echo "</div>";
							echo "</div>";
							
							echo "<div class='grid_2'>";
								echo "<label for='lauf_nebenkosten_abzugsfaehig'>Steuerlich Abzugsfähig</label>";
								echo "<div>";
									echo "<select name='lauf_nebenkosten_abzugsfaehig'>";
										echo "<option value=''>Wählen</option>";
										echo "<option value='ja'>Ja</option>";
										echo "<option value='nein'>Nein</option>";
									echo "</select>";	
								echo "</div>";	
							echo "</div>";
							
							echo "<div class='grid_3'>";
								echo "<label for='lauf_nebenkosten_qm'>Standartwert in € oder €/m<sup>2</sup></label>";
								
								echo "<div class='einheit'>";
										echo "<input type='text' name='lauf_nebenkosten_qm' placeholder='0,00'/>";
										echo "<div class='select-wrapper'>";
											echo "<select name='lauf_nebenkosten_qm_einheit' id='select_qm'>";
												echo "<option value='euro'>€</option>";
												echo "<option value='euro_qm'>€/m<sup>2</sup></option>";
											echo "</select>";
										echo "</div>";
									echo "</div>";		
							echo "</div>";
							
							echo "<div class='grid_2'>";
								echo "<br>";
								echo "<button type='button' class='btn14 js-show-erweiterung-nebenkosten show-erweiterung' >Erweiterung</button>";
							echo "</div>";
							
							echo "<div class='grid_12 ins7 grid_row js-erweiterung-nebenkosten erweiterung'>";
								echo "<div></div>";	
								echo "<div class='grid_3'>";
									echo "<label for='erweiterung_nebenkosten_steigerung_ab'>Steigerung ab Jahr</label>";
									echo "<select name='erweiterung_nebenkosten_steigerung_ab'>";
										for($i = 0; $i<= 10; $i++){
											echo "<option value='$i'>$i</option>";
										}
									echo "</select>";	
								echo "</div>";
								echo "<div class='grid_3'>";
									echo "<label for='erweiterung_nebenkosten_steigerung_alle'>Steigerung alle Jahr</label>";
									echo "<select name='erweiterung_nebenkosten_steigerung_alle'>";
										for($i = 0; $i<= 10; $i++){
											echo "<option value='$i'>$i</option>";
										}
									echo "</select>";	
								echo "</div>";
								echo "<div class='grid_3'>";
									echo "<label for='erweiterung_nebenkosten_steigerung'>Steigerungin € oder %</label>";
									
									echo "<div class='einheit'>";
										echo "<input type='text' name='erweiterung_nebenkosten_steigerung' placeholder='0,00'/>";
										echo "<div class='select-wrapper'>";
											echo "<select name='erweiterung_nebenkosten_steigerung_einheit'>";
												echo "<option value='euro'>€</option>";
												echo "<option value='prozent'>%</option>";
											echo "</select>";
										echo "</div>";
									echo "</div>";
									
								echo "</div>";
							echo "</div>";
						echo "</div>";
						
						echo "<input type='hidden' id='id_2' value='1' />";
						echo "<div id='addFields2'></div>";
						
						echo "<div class='grid_11'>";
						echo "</div>";
						echo "<div class='grid_1'>";
							echo "<h3 class='gruen'>";
								echo "<a href='' class='js-add-form-fields-2'>";
									echo "<i class='fa fa-plus-square fa-2x' aria-hidden='true' ></i>";
								echo "</a>";
							echo "</h3>";
						echo "</div>";
						
					echo "</div>";
					/**/	
					
					//Allgemeine Zusatzgrunddaten
					echo "<div class='grid_12 well2 grid_row zusatzgrunddaten'>";
						echo "<div class='header-blau'><p>Allgemeine Zusatzgrunddaten<span class='js-delete'><img src='images/muell.svg' titel='entfernen'/></span></p></div>";
						
						echo "<div class='grid_3'>";
							echo "<label for='zusatzgrunddaten_zeitraum'>Betrachtungszeitraum in Jahren</label>";
							echo "<div>";
								echo "<input type='number' value='20' name='zusatzgrunddaten_zeitraum' placeholder='20' min='1' max='40' step='1'/>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<div>";
								echo "<label for='zusatzgrunddaten_preissteigerung'>Preissteigerung in Prozent</label>";
								echo "<div class='proz'>";
									echo "<input type='text' name='zusatzgrunddaten_preissteigerung' placeholder='0,00'/>";
								echo "</div>";
							echo "</div>";
							echo "<div>";
								echo "<label for='zusatzgrunddaten_steigerung_jahr_1'>Steigerung alle Jahr</label>";
								echo "<div>";
									echo "<select name='zusatzgrunddaten_steigerung_jahr_1'>";
										for($i = 0; $i<= 10; $i++){
											echo "<option value='$i'>$i</option>";
										}
									echo "</select>";	
								echo "</div>";
							echo "</div>";
						echo "</div>";
						
						echo "<div class='grid_4'>";
							echo "<div>";
								echo "<label for='zusatzgrunddaten_inflationsrate'>Inflationsrate in Prozent</label>";
								echo "<div class='proz'>";
									echo "<input type='text' name='zusatzgrunddaten_inflationsrate' placeholder='0,00'/>";
								echo "</div>";
							echo "</div>";
							echo "<div>";
								echo "<label for='zusatzgrunddaten_steigerung_jahr_2'>Steigerung alle Jahr</label>";
								echo "<div>";
									echo "<select name='zusatzgrunddaten_steigerung_jahr_2'>";
									for($i = 0; $i<= 10; $i++){
										echo "<option value='$i'>$i</option>";
									}
								echo "</select>";	
								echo "</div>";
							echo "</div>";		
						echo "</div>";
					echo "</div>";
					/**/	
					
				echo "</div>";
			echo "</section>";
		echo "</form>";
?>

      </main>
      
<script>
	$( function() {
		
		$( ".datepicker" ).datepicker({
			changeMonth: true,
      		changeYear: true,
			dateFormat: "dd.mm.yy"
		});
		
		$(".js-calendar").click(function(){
			$(this).parent().find('input').focus();
		});
		
		$(".js-delete").click(function(){
			$(this).parent().parent().parent().find(':input').val('');
		});
		
		$("select:not([class*='ui-datepicker'])").on('change', function(ev){
			setSelectBox(ev);	
		});
		
		$("select:not([class*='ui-datepicker'])").change();
		
		function setSelectBox(ev){
			var selectBox = $(ev.target);
			
			if($(selectBox).val() == ''){
				$(selectBox).css('color', '#d9d9d9');
			} else {
				$(selectBox).css('color', '#888')
			}
		}
		
		
		$("#select_qm").on('change', function(ev){
			$("#select_qm").toggleClass( "small" );	
		});
		
		$(".js-show-erweiterung-nebenkosten").on('click', function(ev){
			toggleClass(ev);
		});
		
		function toggleClass(ev){
			$(ev.target).toggleClass( "delete" );
			$(ev.target).parent().parent().find('.js-erweiterung-nebenkosten').toggleClass( "active" );	
		}
		
		$(".js-add-form-fields").on('click', function(ev){
			ev.preventDefault();
			addFormField();
			$("select:not([class*='ui-datepicker'])").on('change', function(ev){
				setSelectBox(ev);	
			});
			initRemoveFormFields();
		});
		
		function addFormField() {
			
			var id = $('#id').val();
			
			var fields = '<div class="grid_12 ins7"><div class="grid_11"></div><div class="grid_1"><h3 class="rot"><a href="" class="js-remove-form-fields"><i class="fa fa-minus-square fa-2x" aria-hidden="true"></i></a></h3></div><div class="grid_12 grid_row"><div></div><div class="grid_5"><label for="nebenkosten_bezeichnung_'+id+'">Bezeichnung</label><div><input type="text" name="nebenkosten_bezeichnung_'+id+'"  id="nebenkosten_bezeichnung_'+id +'" placeholder="Bezeichnung"></div></div><div class="grid_3"><label for="nebenkosten_typ_'+id+'">Typ</label><div><select name="nebenkosten_typ_'+id+'" id="nebenkosten_typ_'+id+'"><option value="">Typ wählen</option><option value="werbungskosten">Werbungskosten</option></select></div></div><div class="grid_2"><label for="nebenkosten_proz_'+id+'">Prozent</label><div class="proz"><input type="text" name="nebenkosten_proz_'+id+'" id="nebenkosten_proz_'+id+'" placeholder="0,00"></div></div><div class="grid_2"><label for="nebenkosten_jahr_'+id+'">Jahr</label><div><input type="text" name="nebenkosten_jahr_'+id+'" id="nebenkosten_jahr_'+id+'" placeholder="Jahr"></div></div></div></div>';
			
			$("#addFields").append(fields);
			$('#id').val(++id);
		}
		
		$(".js-add-form-fields-2").on('click', function(ev){
			ev.preventDefault();
			addFormField2();
			$("select:not([class*='ui-datepicker'])").on('change', function(ev){
				setSelectBox(ev);	
			});
			
			initShowErweiterung();
			initRemoveFormFields();
			$("select:not([class*='ui-datepicker']):not(#select_qm)").change();
			
		});
		
		function initShowErweiterung(){
			$(".js-show-erweiterung-nebenkosten").off('click');
			$(".js-show-erweiterung-nebenkosten").on('click', function(ev){
				toggleClass(ev);
			});
		}
		
		function addFormField2() {
			
			var id = $('#id_2').val();
			
			var fields = '<div class="grid_12 ins7"><div class="grid_11"></div><div class="grid_1"><h3 class="rot"><a href="" class="js-remove-form-fields"><i class="fa fa-minus-square fa-2x" aria-hidden="true"></i></a></h3></div><div class="grid_12 grid_row"><div></div><div class="grid_5"><label for="lauf_nebenkosten_bezeichnung_'+id+'">Bezeichnung</label><div><input type="text" name="lauf_nebenkosten_bezeichnung_'+id+'" placeholder="Bezeichnung"></div></div><div class="grid_2"><label for="lauf_nebenkosten_abzugsfaehig_'+id+'">Steuerlich Abzugsfähig</label><div><select name="lauf_nebenkosten_abzugsfaehig_'+id+'"><option value="">Wählen</option><option value="ja">Ja</option><option value="nein">Nein</option></select></div></div><div class="grid_3"><label for="lauf_nebenkosten_qm_'+id+'">Standartwert in € oder €/m<sup>2</sup></label><div class="einheit"><input type="text" name="lauf_nebenkosten_qm_'+id+'" placeholder="0,00"><div class="select-wrapper"><select name="lauf_nebenkosten_qm_einheit_'+id+'" id="select_qm_'+id+'"><option value="euro">€</option><option value="euro_qm">€/m2</option></select></div></div></div><div class="grid_2"><br><button type="button" class="btn14 js-show-erweiterung-nebenkosten show-erweiterung">Erweiterung</button></div><div class="grid_12 ins7 grid_row js-erweiterung-nebenkosten erweiterung"><div></div><div class="grid_3"><label for="erweiterung_nebenkosten_steigerung_ab_'+id+'">Steigerung ab Jahr</label><select name="erweiterung_nebenkosten_steigerung_ab_'+id+'"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div><div class="grid_3"><label for="erweiterung_nebenkosten_steigerung_alle_'+id+'">Steigerung alle Jahr</label><select name="erweiterung_nebenkosten_steigerung_alle_'+id+'"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div><div class="grid_3"><label for="erweiterung_nebenkosten_steigerung_'+id+'">Steigerungin € oder %</label><div class="einheit"><input type="text" name="erweiterung_nebenkosten_steigerung_'+id+'" placeholder="0,00"><div class="select-wrapper"><select name="erweiterung_nebenkosten_steigerung_einheit_'+id+'"><option value="euro">€</option><option value="prozent">%</option></select></div></div></div></div></div></div>';
			
			$("#addFields2").append(fields);
			$('#id_2').val(++id);
		}
		
		function initRemoveFormFields(){
			$(".js-remove-form-fields").off('click');
			$(".js-remove-form-fields").on('click', function(ev){
				removeFormField(ev);
			});
		}
		
		function removeFormField(ev){
			ev.preventDefault();
			$(ev.target).closest('div.grid_12').remove();
		}
		
		function validate_num(formdata) {
		  formdata.value=formdata.value.replace(/\D/, '' );
		}
		
		$("input[name='zusatzgrunddaten_zeitraum']").keyup(function(ev){
			this.value = this.value.replace(/\D/, '' );
			if(this.value < 1 && this.value != ''){
				this.value = 1;
			} else if(this.value > 40) {
				this.value = 40;
			}
			
		});
		
	} );
</script>
      
      
      
<!-- ==============FOOTER============== -->
                      
<?php      
 include ("inc/end.php");
?>