<?php
define("_BASE_","konto_login.php");  // filename (basename perl&php);
$stamp = filemtime(_BASE_);
$last_touch =  date("d.m.Y", $stamp);
$stunde  = date("H");

function utime ()
{
$time = explode( " ", microtime());
$usec = (double)$time[0];
$sec = (double)$time[1];
return $sec + $usec;
}

$start = utime();
$stamp = time();
$heute = gmdate("d M Y H:i:s" , $stamp);
$tag  = date("d");
$monat  = date("m");
$jahr  = date("Y");

require("global_funcs.php");
require("mycrm/inc/lib_incl_login.php");

include "mycrm/inc/head.inc.php";
//include "mycrm/inc/header_extern.php";
//include "mycrm/nav/nav.php";

?>    

  
<!-- ==============CONTENT============== -->
      <?php
	    $id= $_GET['id'];
	  ?>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Content-->
      <main class="page-content bg-shark-radio">
        <div class="one-page">
          <!-- Login-->
          <section>
            <div class="shell">
              <div class="range">
                <div class="section-110 section-cover range range-xs-center range-xs-middle">
                  <div class="cell-xs-8 cell-sm-6 cell-md-4">
                    <div class="panel section-34 section-sm-41 inset-left-20 inset-right-20 inset-sm-left-20 inset-sm-right-20 inset-lg-left-30 inset-lg-right-30 bg-white shadow-drop-md">
                                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default mdi mdi-account-multiple-outline"></span>
                                <div>
                                  <div class="offset-top-24 text-darker big text-bold">Bitte loggen Sie sich</div>
                                  <p class="text-extra-small text-dark offset-top-4">mit Ihren Daten ein</p>
                                </div>
                      <!-- RD Mailform-->
                      <form id="form1" data-form-output="form-output-global" data-form-type="contact" method="post" action="check_login.php" class="text-left offset-top-30">
                        <div class="form-group">
                          <div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>
                            <input id="login-your-login" placeholder="Benutzer" type="text" name="email" data-constraints="@Required" class="form-control">
                          </div>
                        </div>
                        <div class="form-group offset-top-20">
                          <div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>
                            <input id="login-your-password" placeholder="Passwort" type="password" name="password" data-constraints="@Required" class="form-control">
                          </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-icon btn-block btn-primary offset-top-20" onClick="document.getElementById('form1').submit()">Login <span class="icon mdi mdi-arrow-right-bold-circle-outline"></span></button>
                        <input  type="hidden" name="session" value="<?php echo $sess_id ?>">

                      </form>
					  <?php
                         if (isset($_GET['id']) && $_GET['id'] == "1")
                        {
                      ?>
                          <p class="rot" id="angefordert">Passwort gesperrt !</p>
                      <?php
                        }
                      ?>
                      
                      <!--<div class="offset-top-30 text-sm-left text-dark text-extra-small"><a href="#" class="text-picton-blue">Passwort vergessen?</a>-->
                        <!--<div class="offset-top-0">Haben Sie kein Konto? <a href="#" class="text-picton-blue">Sign up here</a>.</div>-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </section>
            </div>


      </main>
    </div>
    
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>        
        
        
        
      
<!-- ==============FOOTER============== -->
<?php      
// include ("inc/end.php");
?>